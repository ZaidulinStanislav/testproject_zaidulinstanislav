<Q                         ATTEN_METHOD_3     HEIGHT_GRAD    Twirl_Y 
   VOLUME_FOG     VOLUME_SHADOWS     _FOG_VOLUME_NOISE       ^\  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _ProjectionParams;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixInvV[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec3 L;
in highp vec4 in_POSITION0;
in highp vec4 in_TEXCOORD0;
out highp vec3 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec3 vs_TEXCOORD3;
out highp vec3 vs_TEXCOORD4;
out highp vec3 vs_TEXCOORD5;
out highp vec3 vs_TEXCOORD6;
out highp vec2 vs_TEXCOORD7;
out highp vec3 vs_TEXCOORD8;
out highp vec3 vs_TEXCOORD9;
vec4 u_xlat0;
vec4 u_xlat1;
vec4 u_xlat2;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat1;
    vs_TEXCOORD0.xyz = u_xlat0.xyz;
    u_xlat1.y = u_xlat1.y * _ProjectionParams.x;
    u_xlat2.xzw = u_xlat1.xwy * vec3(0.5, 0.5, 0.5);
    vs_TEXCOORD1.zw = u_xlat1.zw;
    vs_TEXCOORD1.xy = u_xlat2.zz + u_xlat2.xw;
    vs_TEXCOORD2.xyz = in_POSITION0.xyz;
    u_xlat1.xyz = u_xlat0.yyy * hlslcc_mtx4x4unity_MatrixV[1].xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_MatrixV[0].xyz * u_xlat0.xxx + u_xlat1.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_MatrixV[2].xyz * u_xlat0.zzz + u_xlat1.xyz;
    vs_TEXCOORD3.xyz = hlslcc_mtx4x4unity_MatrixV[3].xyz * u_xlat0.www + u_xlat1.xyz;
    vs_TEXCOORD6.xyz = u_xlat0.xyz + (-_WorldSpaceCameraPos.xyz);
    u_xlat0.xyz = _WorldSpaceCameraPos.yyy * hlslcc_mtx4x4unity_WorldToObject[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[0].xyz * _WorldSpaceCameraPos.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[2].xyz * _WorldSpaceCameraPos.zzz + u_xlat0.xyz;
    vs_TEXCOORD4.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_WorldToObject[3].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[1].xyz * L.yyy;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[0].xyz * L.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[2].xyz * L.zzz + u_xlat0.xyz;
    vs_TEXCOORD5.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_WorldToObject[3].xyz;
    vs_TEXCOORD7.xy = in_TEXCOORD0.xy;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[1].xyz * hlslcc_mtx4x4unity_MatrixInvV[2].yyy;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[0].xyz * hlslcc_mtx4x4unity_MatrixInvV[2].xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[2].xyz * hlslcc_mtx4x4unity_MatrixInvV[2].zzz + u_xlat0.xyz;
    vs_TEXCOORD8.xyz = hlslcc_mtx4x4unity_WorldToObject[3].xyz * hlslcc_mtx4x4unity_MatrixInvV[2].www + u_xlat0.xyz;
    vs_TEXCOORD9.xyz = vec3(0.0, -1.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _Time;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _ZBufferParams;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	float Collisions;
uniform 	mediump vec4 _AmbientHeightAbsorption;
uniform 	mediump vec4 _VerticalGradientParams;
uniform 	int STEP_COUNT;
uniform 	int _ProxyVolume;
uniform 	int _AmbientAffectsFogColor;
uniform 	int Octaves;
uniform 	vec4 _Color;
uniform 	vec4 _FogColor;
uniform 	vec4 _BoxMin;
uniform 	vec4 _BoxMax;
uniform 	vec4 Stretch;
uniform 	vec4 _AmbientColor;
uniform 	vec4 VolumeSize;
uniform 	vec4 _VolumePosition;
uniform 	vec3 L;
uniform 	vec3 Speed;
uniform 	mediump float DetailTiling;
uniform 	mediump float _PrimitiveCutout;
uniform 	mediump float _LightExposure;
uniform 	mediump float _PushAlpha;
uniform 	mediump float _DetailMaskingThreshold;
uniform 	mediump float _OptimizationFactor;
uniform 	mediump float _BaseRelativeSpeed;
uniform 	mediump float _DetailRelativeSpeed;
uniform 	mediump float _NoiseDetailRange;
uniform 	mediump float _Curl;
uniform 	mediump float BaseTiling;
uniform 	mediump float _Cutoff;
uniform 	mediump float Coverage;
uniform 	mediump float NoiseDensity;
uniform 	mediump float _Vortex;
uniform 	mediump float _RotationSpeed;
uniform 	mediump float _Rotation;
uniform 	mediump float DetailDistance;
uniform 	mediump float _SceneIntersectionSoftness;
uniform 	mediump float _Visibility;
uniform 	mediump float _3DNoiseScale;
uniform 	mediump float _RayStep;
uniform 	mediump float gain;
uniform 	mediump float threshold;
uniform 	mediump float FadeDistance;
UNITY_LOCATION(0) uniform highp sampler2D _CameraDepthTexture;
UNITY_LOCATION(1) uniform mediump sampler3D _NoiseVolume;
UNITY_LOCATION(2) uniform mediump sampler2D LightshaftTex;
in highp vec4 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
in highp vec3 vs_TEXCOORD3;
in highp vec3 vs_TEXCOORD4;
layout(location = 0) out highp vec4 SV_Target0;
vec3 u_xlat0;
vec3 u_xlat1;
bool u_xlatb1;
vec3 u_xlat2;
vec3 u_xlat3;
vec2 u_xlat4;
vec3 u_xlat5;
mediump vec3 u_xlat16_6;
vec3 u_xlat7;
vec3 u_xlat8;
vec3 u_xlat9;
mediump vec3 u_xlat16_10;
mediump float u_xlat16_11;
vec4 u_xlat12;
float u_xlat13;
mediump float u_xlat16_13;
vec2 u_xlat14;
vec4 u_xlat15;
mediump float u_xlat16_15;
vec4 u_xlat16;
mediump float u_xlat16_16;
bool u_xlatb16;
vec3 u_xlat17;
mediump vec3 u_xlat16_18;
vec3 u_xlat19;
bool u_xlatb19;
vec4 u_xlat20;
mediump vec3 u_xlat16_21;
vec2 u_xlat22;
mediump vec3 u_xlat16_23;
vec2 u_xlat25;
bool u_xlatb25;
mediump vec3 u_xlat16_35;
mediump float u_xlat16_37;
vec2 u_xlat38;
mediump float u_xlat16_42;
vec3 u_xlat43;
bool u_xlatb43;
float u_xlat49;
bool u_xlatb49;
bvec2 u_xlatb52;
mediump float u_xlat16_54;
mediump float u_xlat16_61;
int u_xlati62;
bool u_xlatb62;
float u_xlat64;
mediump float u_xlat16_64;
int u_xlati64;
mediump float u_xlat16_66;
float u_xlat72;
float u_xlat73;
float u_xlat74;
bool u_xlatb75;
int u_xlati76;
int u_xlati77;
mediump float u_xlat16_78;
int u_xlati79;
float u_xlat80;
float u_xlat81;
mediump float u_xlat16_82;
mediump float u_xlat16_85;
float u_xlat86;
bool u_xlatb86;
float u_xlat87;
bool u_xlatb87;
float u_xlat89;
mediump float u_xlat16_89;
bool u_xlatb89;
mediump float u_xlat16_90;
float u_xlat92;
mediump float u_xlat16_92;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD2.xyz + (-vs_TEXCOORD4.xyz);
    u_xlat72 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat72 = inversesqrt(u_xlat72);
    u_xlat0.xyz = vec3(u_xlat72) * u_xlat0.xyz;
    u_xlat1.xyz = vec3(1.0, 1.0, 1.0) / u_xlat0.xyz;
    u_xlat2.xyz = (-vs_TEXCOORD4.xyz) + _BoxMin.xyz;
    u_xlat2.xyz = u_xlat1.xyz * u_xlat2.xyz;
    u_xlat3.xyz = (-vs_TEXCOORD4.xyz) + _BoxMax.xyz;
    u_xlat1.xyz = u_xlat1.xyz * u_xlat3.xyz;
    u_xlat3.xyz = min(u_xlat2.xyz, u_xlat1.xyz);
    u_xlat1.xyz = max(u_xlat2.xyz, u_xlat1.xyz);
    u_xlat2.xy = max(u_xlat3.yz, u_xlat3.xx);
    u_xlat72 = max(u_xlat2.y, u_xlat2.x);
    u_xlat1.xy = min(u_xlat1.yz, u_xlat1.xx);
    u_xlat1.x = min(u_xlat1.y, u_xlat1.x);
#ifdef UNITY_ADRENO_ES3
    u_xlatb25 = !!(u_xlat1.x<u_xlat72);
#else
    u_xlatb25 = u_xlat1.x<u_xlat72;
#endif
    if(((int(u_xlatb25) * int(0xffffffffu)))!=0){discard;}
#ifdef UNITY_ADRENO_ES3
    u_xlatb25 = !!(u_xlat72<0.0);
#else
    u_xlatb25 = u_xlat72<0.0;
#endif
    u_xlat72 = (u_xlatb25) ? 0.0 : u_xlat72;
    u_xlat25.xy = vs_TEXCOORD1.xy / vs_TEXCOORD1.ww;
    u_xlat25.x = texture(_CameraDepthTexture, u_xlat25.xy).x;
    u_xlat25.x = _ZBufferParams.z * u_xlat25.x + _ZBufferParams.w;
    u_xlat25.x = float(1.0) / u_xlat25.x;
    u_xlat49 = dot(vs_TEXCOORD3.xyz, vs_TEXCOORD3.xyz);
    u_xlat49 = inversesqrt(u_xlat49);
    u_xlat49 = u_xlat49 * vs_TEXCOORD3.z;
    u_xlat25.x = u_xlat25.x / u_xlat49;
    u_xlat2.xyz = u_xlat0.xyz * vec3(u_xlat72) + vs_TEXCOORD4.xyz;
    u_xlat0.xyz = u_xlat0.xyz * u_xlat1.xxx + vs_TEXCOORD4.xyz;
    u_xlat0.xyz = (-u_xlat2.xyz) + u_xlat0.xyz;
    u_xlat49 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat73 = sqrt(u_xlat49);
    u_xlat3.xyz = _Time.xxx * Speed.xyz;
    u_xlat74 = VolumeSize.y / VolumeSize.x;
    u_xlat4.xy = vec2(u_xlat74) * L.xz;
    u_xlat49 = inversesqrt(u_xlat49);
    u_xlat0.xyz = u_xlat0.xyz * vec3(u_xlat49);
    u_xlat5.xyz = Stretch.xyz * vec3(vec3(_3DNoiseScale, _3DNoiseScale, _3DNoiseScale));
#ifdef UNITY_ADRENO_ES3
    u_xlatb49 = !!(Collisions!=0.0);
#else
    u_xlatb49 = Collisions!=0.0;
#endif
    u_xlat74 = max(u_xlat1.x, u_xlat72);
    u_xlat72 = min(u_xlat1.x, u_xlat72);
    u_xlat16_6.xy = (-_VerticalGradientParams.xz) + _VerticalGradientParams.yw;
    u_xlat7.xyz = (-_BoxMin.yzx) + _BoxMax.yzx;
#ifdef UNITY_ADRENO_ES3
    u_xlatb1 = !!(_PrimitiveCutout<1.0);
#else
    u_xlatb1 = _PrimitiveCutout<1.0;
#endif
#ifdef UNITY_ADRENO_ES3
    u_xlatb75 = !!(0.00999999978<Coverage);
#else
    u_xlatb75 = 0.00999999978<Coverage;
#endif
    u_xlat16_54 = max(threshold, 0.0);
    u_xlat16_78 = _NoiseDetailRange + 1.0;
    u_xlat8.xyz = u_xlat3.xyz * vec3(_DetailRelativeSpeed);
    u_xlat9.xyz = u_xlat3.xyz * vec3(vec3(_BaseRelativeSpeed, _BaseRelativeSpeed, _BaseRelativeSpeed));
    u_xlat16_10.xyz = _AmbientColor.xyz * vec3(0.99999994, 0.99999994, 0.99999994);
    u_xlat16_82 = (-_AmbientHeightAbsorption.x) + _AmbientHeightAbsorption.y;
    u_xlatb52.xy = equal(_AmbientHeightAbsorption.xyxy, vec4(-1.0, -1.0, -1.0, -1.0)).xy;
    u_xlatb52.x = u_xlatb52.y && u_xlatb52.x;
    u_xlat16_11 = _Cutoff * -49.0 + 50.0;
    u_xlati76 = _ProxyVolume;
    u_xlati77 = _AmbientAffectsFogColor;
    u_xlati79 = Octaves;
    u_xlat80 = u_xlat73;
    u_xlat12.x = float(0.0);
    u_xlat12.y = float(0.0);
    u_xlat12.z = float(0.0);
    u_xlat12.w = float(0.0);
    u_xlat16_35.x = float(0.0);
    u_xlat16_35.y = float(0.0);
    u_xlat16_35.z = float(0.0);
    u_xlat16_13 = 1.0;
    u_xlat81 = 0.0;
    u_xlat14.x = _RayStep;
    u_xlat14.y = 1.40129846e-45;
    while(true){
#ifdef UNITY_ADRENO_ES3
        u_xlatb62 = !!(floatBitsToInt(u_xlat14.y)<STEP_COUNT);
#else
        u_xlatb62 = floatBitsToInt(u_xlat14.y)<STEP_COUNT;
#endif
#ifdef UNITY_ADRENO_ES3
        u_xlatb86 = !!(0.0<u_xlat80);
#else
        u_xlatb86 = 0.0<u_xlat80;
#endif
        u_xlatb62 = u_xlatb86 && u_xlatb62;
        if(!u_xlatb62){break;}
        u_xlati62 = floatBitsToInt(u_xlat14.y) * floatBitsToInt(u_xlat14.y);
        u_xlati62 = floatBitsToInt(u_xlat14.y) * u_xlati62;
        u_xlat16_37 = float(u_xlati62);
        u_xlat16_37 = u_xlat16_37 * _OptimizationFactor + 1.0;
        u_xlat38.y = u_xlat16_37 * u_xlat14.x;
        u_xlat15.xyz = u_xlat0.xyz * vec3(u_xlat81) + u_xlat2.xyz;
        u_xlat16.xyz = u_xlat15.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
        u_xlat16.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * u_xlat15.xxx + u_xlat16.xyz;
        u_xlat16.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * u_xlat15.zzz + u_xlat16.xyz;
        u_xlat16.xyz = u_xlat16.xyz + _VolumePosition.xyz;
        u_xlat17.xyz = u_xlat16.xyz + (-_WorldSpaceCameraPos.xyz);
        u_xlat86 = dot(u_xlat17.xyz, u_xlat17.xyz);
        u_xlat86 = sqrt(u_xlat86);
        u_xlat16_61 = u_xlat86 / FadeDistance;
#ifdef UNITY_ADRENO_ES3
        u_xlat16_61 = min(max(u_xlat16_61, 0.0), 1.0);
#else
        u_xlat16_61 = clamp(u_xlat16_61, 0.0, 1.0);
#endif
        u_xlat16_61 = (-u_xlat16_61) + 1.0;
#ifdef UNITY_ADRENO_ES3
        u_xlatb87 = !!(u_xlat16_61<0.00100000005);
#else
        u_xlatb87 = u_xlat16_61<0.00100000005;
#endif
        if(u_xlatb87){
            break;
        }
        if(u_xlatb49){
            u_xlat17.xyz = u_xlat15.xyz + (-vs_TEXCOORD4.xyz);
            u_xlat87 = dot(u_xlat17.xyz, u_xlat17.xyz);
            u_xlat87 = sqrt(u_xlat87);
            u_xlat87 = abs(u_xlat25.x) + (-u_xlat87);
            u_xlat13 = u_xlat87 * _SceneIntersectionSoftness;
#ifdef UNITY_ADRENO_ES3
            u_xlat13 = min(max(u_xlat13, 0.0), 1.0);
#else
            u_xlat13 = clamp(u_xlat13, 0.0, 1.0);
#endif
#ifdef UNITY_ADRENO_ES3
            u_xlatb87 = !!(u_xlat13<0.00999999978);
#else
            u_xlatb87 = u_xlat13<0.00999999978;
#endif
            if(u_xlatb87){
                break;
            }
            u_xlat16_13 = u_xlat13;
        }
        u_xlat17.xyz = u_xlat15.yzx / u_xlat7.xyz;
        u_xlat17.xyz = u_xlat17.xyz + vec3(0.5, -0.5, -0.5);
        u_xlat16.xyz = (-u_xlat16.xyz) + _WorldSpaceCameraPos.xyz;
        u_xlat87 = dot(u_xlat16.xyz, u_xlat16.xyz);
        u_xlat87 = sqrt(u_xlat87);
        u_xlat16.x = min(u_xlat74, u_xlat87);
        u_xlat87 = min(u_xlat72, u_xlat87);
        u_xlat87 = (-u_xlat87) + u_xlat16.x;
        u_xlat87 = (-u_xlat87) / _Visibility;
        u_xlat87 = u_xlat87 * 7.21347523;
        u_xlat87 = exp2(u_xlat87);
        u_xlat87 = (-u_xlat87) + 1.0;
        u_xlat87 = max(u_xlat87, 0.0);
        u_xlat16_85 = u_xlat16_13 * u_xlat87;
        u_xlat16_18.x = u_xlat16_61 * gain;
        u_xlat16_18.x = u_xlat16_13 * u_xlat16_18.x;
        u_xlat16.xy = u_xlat17.xx + (-_VerticalGradientParams.xz);
        u_xlat16.xy = u_xlat16.xy / u_xlat16_6.xy;
#ifdef UNITY_ADRENO_ES3
        u_xlat16.xy = min(max(u_xlat16.xy, 0.0), 1.0);
#else
        u_xlat16.xy = clamp(u_xlat16.xy, 0.0, 1.0);
#endif
        u_xlat16_42 = u_xlat16.y * u_xlat16.x;
        u_xlat16_18.x = u_xlat16_42 * u_xlat16_18.x;
#ifdef UNITY_ADRENO_ES3
        u_xlatb87 = !!(0.0<u_xlat16_13);
#else
        u_xlatb87 = 0.0<u_xlat16_13;
#endif
#ifdef UNITY_ADRENO_ES3
        u_xlatb16 = !!(0.0<u_xlat16_18.x);
#else
        u_xlatb16 = 0.0<u_xlat16_18.x;
#endif
        u_xlatb87 = u_xlatb87 && u_xlatb16;
        u_xlatb87 = u_xlatb1 && u_xlatb87;
        if(u_xlatb87){
            u_xlat15.xyz = u_xlat5.xyz * u_xlat15.xyz;
            u_xlat16_42 = u_xlat86 / DetailDistance;
#ifdef UNITY_ADRENO_ES3
            u_xlat16_42 = min(max(u_xlat16_42, 0.0), 1.0);
#else
            u_xlat16_42 = clamp(u_xlat16_42, 0.0, 1.0);
#endif
            u_xlat16_42 = (-u_xlat16_42) + 1.0;
            u_xlat86 = dot(u_xlat15.xyz, u_xlat15.xyz);
            u_xlat86 = sqrt(u_xlat86);
            u_xlat86 = u_xlat86 * _Vortex + _Rotation;
            u_xlat86 = _RotationSpeed * _Time.x + u_xlat86;
            u_xlat16.x = sin(u_xlat86);
            u_xlat19.x = cos(u_xlat86);
            u_xlat20.x = sin((-u_xlat86));
            u_xlat20.y = u_xlat19.x;
            u_xlat20.z = u_xlat16.x;
            u_xlat16.x = dot(u_xlat15.xz, u_xlat20.yz);
            u_xlat16.y = dot(u_xlat15.xz, u_xlat20.xy);
            if(u_xlatb75){
                u_xlat16.z = u_xlat15.y;
                u_xlat15.xzw = u_xlat16.xzy * vec3(BaseTiling);
                u_xlat19.xyz = u_xlat15.xzw;
                u_xlat86 = 0.0;
                for(int u_xlati_loop_1 = 0 ; u_xlati_loop_1<u_xlati79 ; u_xlati_loop_1++)
                {
                    u_xlat89 = float(u_xlati_loop_1);
                    u_xlat20.xy = vec2(u_xlat89) * vec2(0.0599999987, 0.150000006) + vec2(1.0, 1.0);
                    u_xlat20.xzw = u_xlat19.xyz / u_xlat20.xxx;
                    u_xlat19.xyz = u_xlat9.xyz * u_xlat20.yyy + u_xlat20.xzw;
                    u_xlat16_89 = textureLod(_NoiseVolume, u_xlat19.xyz, 0.0).x;
                    u_xlat86 = u_xlat86 + u_xlat16_89;
                }
                u_xlat15.x = float(u_xlati79);
                u_xlat15.x = u_xlat86 / u_xlat15.x;
                u_xlat16_15 = u_xlat15.x;
            } else {
                u_xlat16_15 = 0.0;
            }
            u_xlat16_18.x = u_xlat16_18.x * Coverage;
            u_xlat16_64 = u_xlat16_15 * u_xlat16_18.x;
            u_xlat16_89 = u_xlat16_15 * u_xlat16_18.x + -0.5;
            u_xlat16_89 = u_xlat16_89 * u_xlat16_54 + 0.5;
#ifdef UNITY_ADRENO_ES3
            u_xlat16_89 = min(max(u_xlat16_89, 0.0), 1.0);
#else
            u_xlat16_89 = clamp(u_xlat16_89, 0.0, 1.0);
#endif
            u_xlat64 = u_xlat16_64 * u_xlat16_89;
#ifdef UNITY_ADRENO_ES3
            u_xlat64 = min(max(u_xlat64, 0.0), 1.0);
#else
            u_xlat64 = clamp(u_xlat64, 0.0, 1.0);
#endif
            u_xlat16_18.x = (-u_xlat64) * _DetailMaskingThreshold + 1.0;
#ifdef UNITY_ADRENO_ES3
            u_xlat16_18.x = min(max(u_xlat16_18.x, 0.0), 1.0);
#else
            u_xlat16_18.x = clamp(u_xlat16_18.x, 0.0, 1.0);
#endif
#ifdef UNITY_ADRENO_ES3
            u_xlatb89 = !!(0.0<u_xlat16_42);
#else
            u_xlatb89 = 0.0<u_xlat16_42;
#endif
#ifdef UNITY_ADRENO_ES3
            u_xlatb19 = !!(0.0<u_xlat64);
#else
            u_xlatb19 = 0.0<u_xlat64;
#endif
            u_xlatb89 = u_xlatb89 && u_xlatb19;
#ifdef UNITY_ADRENO_ES3
            u_xlatb19 = !!(0.0<u_xlat16_18.x);
#else
            u_xlatb19 = 0.0<u_xlat16_18.x;
#endif
            u_xlatb89 = u_xlatb89 && u_xlatb19;
            if(u_xlatb89){
                u_xlat16_66 = u_xlat16_42 * u_xlat16_18.x;
                u_xlat16.w = u_xlat15.y;
                u_xlat19.xyz = u_xlat16.xwy * vec3(DetailTiling) + u_xlat8.xyz;
                u_xlat16_89 = textureLod(_NoiseVolume, u_xlat19.xyz, 0.0).x;
                u_xlat16_89 = u_xlat16_89 * u_xlat16_66;
#ifdef UNITY_ADRENO_ES3
                u_xlatb19 = !!(1<u_xlati79);
#else
                u_xlatb19 = 1<u_xlati79;
#endif
                if(u_xlatb19){
                    u_xlat16.xyw = u_xlat16.xwy * vec3(0.5, 0.5, 0.5);
                    u_xlat16.xyw = u_xlat16.xyw * vec3(DetailTiling) + vec3(0.5, 0.5, 0.5);
                    u_xlat16_66 = u_xlat16_89 * _Curl;
                    u_xlat16_66 = dot(vec2(u_xlat16_66), u_xlat16_18.xx);
                    u_xlat16.xyw = u_xlat16.xyw + vec3(u_xlat16_66);
                    u_xlat16.xyw = u_xlat3.xyz * vec3(_DetailRelativeSpeed) + u_xlat16.xyw;
                    u_xlat16.xyw = u_xlat16.xyw + vec3(-1.0, -1.0, -1.0);
                    u_xlat16_16 = textureLod(_NoiseVolume, u_xlat16.xyw, 0.0).x;
                    u_xlat16_16 = u_xlat16_16 * u_xlat16_42;
                    u_xlat16_16 = u_xlat16_16 * 1.5;
                    u_xlat16_16 = u_xlat16_16 * u_xlat16_18.x + u_xlat16_89;
                    u_xlat16_16 = u_xlat16_16;
                } else {
                    u_xlat16_16 = u_xlat16_89;
                }
                u_xlat16_89 = u_xlat16_16 * 0.5;
#ifdef UNITY_ADRENO_ES3
                u_xlat16_89 = min(max(u_xlat16_89, 0.0), 1.0);
#else
                u_xlat16_89 = clamp(u_xlat16_89, 0.0, 1.0);
#endif
                u_xlat16_89 = u_xlat16_89;
            } else {
                u_xlat16_89 = 0.0;
            }
            u_xlat19.x = (-u_xlat16_89) * _NoiseDetailRange + u_xlat64;
            u_xlat19.x = u_xlat16_78 * u_xlat19.x;
            u_xlat19.x = u_xlat19.x * NoiseDensity;
#ifdef UNITY_ADRENO_ES3
            u_xlat19.x = min(max(u_xlat19.x, 0.0), 1.0);
#else
            u_xlat19.x = clamp(u_xlat19.x, 0.0, 1.0);
#endif
#ifdef UNITY_ADRENO_ES3
            u_xlatb43 = !!(0.0<u_xlat19.x);
#else
            u_xlatb43 = 0.0<u_xlat19.x;
#endif
            u_xlat19.x = u_xlat16_61 * u_xlat19.x;
            u_xlat19.x = u_xlatb43 ? u_xlat19.x : float(0.0);
        } else {
            u_xlat19.x = 0.0;
        }
#ifdef UNITY_ADRENO_ES3
        u_xlatb43 = !!(u_xlati76==1);
#else
        u_xlatb43 = u_xlati76==1;
#endif
        if(u_xlatb43){
            u_xlat16_18.xyz = u_xlat16_10.xyz;
            u_xlat16_21.xyz = u_xlat16_10.xyz;
        } else {
            u_xlat16_18.xyz = _AmbientColor.xyz;
            u_xlat16_21.xyz = _AmbientColor.xyz;
        }
        u_xlat43.x = u_xlat17.x + (-_AmbientHeightAbsorption.x);
        u_xlat43.x = u_xlat43.x / u_xlat16_82;
#ifdef UNITY_ADRENO_ES3
        u_xlat43.x = min(max(u_xlat43.x, 0.0), 1.0);
#else
        u_xlat43.x = clamp(u_xlat43.x, 0.0, 1.0);
#endif
        u_xlat16_61 = u_xlat43.x * -1.44269502;
        u_xlat16_61 = exp2(u_xlat16_61);
        u_xlat16_61 = (-u_xlat16_61) + 1.0;
        u_xlat16_61 = (u_xlatb52.x) ? 1.0 : u_xlat16_61;
        u_xlat43.x = u_xlat16_13 * u_xlat19.x;
        u_xlat20.xyz = u_xlat16_21.xyz * u_xlat43.xxx;
        u_xlat43.xyz = u_xlat16_35.xyz * u_xlat43.xxx;
        u_xlat16_90 = (-u_xlat17.x) + 1.0;
        u_xlat22.xy = u_xlat4.xy * vec2(u_xlat16_90) + u_xlat17.zy;
        u_xlat16_23.xy = max(u_xlat22.xy, vec2(-1.0, -1.0));
        u_xlat16_23.xy = min(u_xlat16_23.xy, vec2(0.0, 0.0));
        u_xlat16_92 = textureLod(LightshaftTex, u_xlat16_23.xy, 0.0).x;
        u_xlat16_90 = u_xlat16_11 * u_xlat16_92;
#ifdef UNITY_ADRENO_ES3
        u_xlat16_90 = min(max(u_xlat16_90, 0.0), 1.0);
#else
        u_xlat16_90 = clamp(u_xlat16_90, 0.0, 1.0);
#endif
        u_xlat16_90 = (-u_xlat16_90) + 1.0;
        u_xlat16_23.xyz = vec3(u_xlat16_90) * u_xlat20.xyz;
        u_xlat16_23.xyz = vec3(u_xlat16_61) * u_xlat16_23.xyz;
        u_xlat16_35.xyz = vec3(u_xlat16_61) * u_xlat43.xyz;
        u_xlat16_18.xyz = u_xlat16_18.xyz * _FogColor.xyz;
        u_xlat16_18.xyz = (int(u_xlati77) != 0) ? u_xlat16_18.xyz : _FogColor.xyz;
        u_xlat20.x = u_xlat16_90 + _AmbientColor.w;
        u_xlat20.xyz = u_xlat16_18.xyz * u_xlat20.xxx + (-u_xlat16_23.xyz);
        u_xlat20.xyz = vec3(u_xlat16_85) * u_xlat20.xyz + u_xlat16_23.xyz;
        u_xlat92 = u_xlat19.x * u_xlat16_13 + u_xlat16_85;
        u_xlat92 = min(u_xlat92, 1.0);
        u_xlat16_18.xyz = u_xlat43.xyz * vec3(u_xlat16_61) + u_xlat20.xyz;
        u_xlat43.xyz = u_xlat16_18.xyz * _Color.xyz;
        u_xlat20.xyz = u_xlat19.xxx * u_xlat16_21.xyz;
        u_xlat15.xyz = u_xlat43.xyz * vec3(vec3(_LightExposure, _LightExposure, _LightExposure)) + u_xlat20.xyz;
        u_xlat15.w = u_xlat92 * _PushAlpha;
        u_xlat19.x = (-u_xlat12.w) + 1.0;
        u_xlat15 = u_xlat15 * u_xlat19.xxxx + u_xlat12;
#ifdef UNITY_ADRENO_ES3
        u_xlatb19 = !!(0.999000013<u_xlat15.w);
#else
        u_xlatb19 = 0.999000013<u_xlat15.w;
#endif
        if(u_xlatb19){
            u_xlat12 = u_xlat15;
            break;
        }
        u_xlat12 = u_xlat15;
        u_xlat38.x = intBitsToFloat(floatBitsToInt(u_xlat14.y) + 1);
        u_xlat81 = u_xlat14.x * u_xlat16_37 + u_xlat81;
        u_xlat80 = (-u_xlat14.x) * u_xlat16_37 + u_xlat80;
        u_xlat14.xy = u_xlat38.yx;
    }
    u_xlat0.xyz = log2(u_xlat12.xyz);
    u_xlat0.xyz = u_xlat0.xyz * vec3(0.454545468, 0.454545468, 0.454545468);
    u_xlat0.xyz = exp2(u_xlat0.xyz);
    SV_Target0.w = u_xlat12.w;
#ifdef UNITY_ADRENO_ES3
    SV_Target0.w = min(max(SV_Target0.w, 0.0), 1.0);
#else
    SV_Target0.w = clamp(SV_Target0.w, 0.0, 1.0);
#endif
    u_xlat16_6.xyz = min(u_xlat0.xyz, vec3(65504.0, 65504.0, 65504.0));
    SV_Target0.xyz = u_xlat16_6.xyz;
    return;
}

#endif
                               $Globals�  .      _Time                            _WorldSpaceCameraPos                        _ZBufferParams                        
   Collisions                    p      _AmbientHeightAbsorption                  �      _VerticalGradientParams                   �   
   STEP_COUNT                   �      _ProxyVolume                 �      _AmbientAffectsFogColor                  �      Octaves                  �      _Color                    �   	   _FogColor                     �      _BoxMin                   �      _BoxMax                   �      Stretch                   �      _AmbientColor                        
   VolumeSize                         _VolumePosition                         L                     0     Speed                     D     DetailTiling                  P     _PrimitiveCutout                  T     _LightExposure                    X  
   _PushAlpha                    \     _DetailMaskingThreshold                   `     _OptimizationFactor                   d     _BaseRelativeSpeed                    h     _DetailRelativeSpeed                  l     _NoiseDetailRange                     p     _Curl                     t  
   BaseTiling                    x     _Cutoff                   |     Coverage                  �     NoiseDensity                  �     _Vortex                   �     _RotationSpeed                    �  	   _Rotation                     �     DetailDistance                    �     _SceneIntersectionSoftness                    �     _Visibility                   �     _3DNoiseScale                     �     _RayStep                  �     gain                  �  	   threshold                     �     FadeDistance                  �     unity_ObjectToWorld                  0          $Globalsl        _WorldSpaceCameraPos                         _ProjectionParams                           L                     `     unity_ObjectToWorld                         unity_WorldToObject                  `      unity_MatrixV                    �      unity_MatrixInvV                 �      unity_MatrixVP                                _CameraDepthTexture                   _NoiseVolume                LightshaftTex                