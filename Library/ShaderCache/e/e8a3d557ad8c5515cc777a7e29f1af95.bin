<Q                         ATTEN_METHOD_2     HEIGHT_GRAD    Twirl_X    _FOG_VOLUME_NOISE       BS  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _ProjectionParams;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixInvV[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec3 L;
in highp vec4 in_POSITION0;
in highp vec4 in_TEXCOORD0;
out highp vec3 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec3 vs_TEXCOORD3;
out highp vec3 vs_TEXCOORD4;
out highp vec3 vs_TEXCOORD5;
out highp vec3 vs_TEXCOORD6;
out highp vec2 vs_TEXCOORD7;
out highp vec3 vs_TEXCOORD8;
out highp vec3 vs_TEXCOORD9;
vec4 u_xlat0;
vec4 u_xlat1;
vec4 u_xlat2;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat1;
    vs_TEXCOORD0.xyz = u_xlat0.xyz;
    u_xlat1.y = u_xlat1.y * _ProjectionParams.x;
    u_xlat2.xzw = u_xlat1.xwy * vec3(0.5, 0.5, 0.5);
    vs_TEXCOORD1.zw = u_xlat1.zw;
    vs_TEXCOORD1.xy = u_xlat2.zz + u_xlat2.xw;
    vs_TEXCOORD2.xyz = in_POSITION0.xyz;
    u_xlat1.xyz = u_xlat0.yyy * hlslcc_mtx4x4unity_MatrixV[1].xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_MatrixV[0].xyz * u_xlat0.xxx + u_xlat1.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_MatrixV[2].xyz * u_xlat0.zzz + u_xlat1.xyz;
    vs_TEXCOORD3.xyz = hlslcc_mtx4x4unity_MatrixV[3].xyz * u_xlat0.www + u_xlat1.xyz;
    vs_TEXCOORD6.xyz = u_xlat0.xyz + (-_WorldSpaceCameraPos.xyz);
    u_xlat0.xyz = _WorldSpaceCameraPos.yyy * hlslcc_mtx4x4unity_WorldToObject[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[0].xyz * _WorldSpaceCameraPos.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[2].xyz * _WorldSpaceCameraPos.zzz + u_xlat0.xyz;
    vs_TEXCOORD4.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_WorldToObject[3].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[1].xyz * L.yyy;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[0].xyz * L.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[2].xyz * L.zzz + u_xlat0.xyz;
    vs_TEXCOORD5.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_WorldToObject[3].xyz;
    vs_TEXCOORD7.xy = in_TEXCOORD0.xy;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[1].xyz * hlslcc_mtx4x4unity_MatrixInvV[2].yyy;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[0].xyz * hlslcc_mtx4x4unity_MatrixInvV[2].xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[2].xyz * hlslcc_mtx4x4unity_MatrixInvV[2].zzz + u_xlat0.xyz;
    vs_TEXCOORD8.xyz = hlslcc_mtx4x4unity_WorldToObject[3].xyz * hlslcc_mtx4x4unity_MatrixInvV[2].www + u_xlat0.xyz;
    vs_TEXCOORD9.xyz = vec3(0.0, -1.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _Time;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _ZBufferParams;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	float Collisions;
uniform 	mediump vec4 _AmbientHeightAbsorption;
uniform 	mediump vec4 _VerticalGradientParams;
uniform 	int STEP_COUNT;
uniform 	int _ProxyVolume;
uniform 	int Octaves;
uniform 	vec4 _Color;
uniform 	vec4 _BoxMin;
uniform 	vec4 _BoxMax;
uniform 	vec4 Stretch;
uniform 	vec4 _AmbientColor;
uniform 	vec4 _VolumePosition;
uniform 	vec3 Speed;
uniform 	mediump float DetailTiling;
uniform 	mediump float _PrimitiveCutout;
uniform 	mediump float _LightExposure;
uniform 	mediump float _PushAlpha;
uniform 	mediump float _DetailMaskingThreshold;
uniform 	mediump float _OptimizationFactor;
uniform 	mediump float _BaseRelativeSpeed;
uniform 	mediump float _DetailRelativeSpeed;
uniform 	mediump float _NoiseDetailRange;
uniform 	mediump float _Curl;
uniform 	mediump float BaseTiling;
uniform 	mediump float Coverage;
uniform 	mediump float NoiseDensity;
uniform 	mediump float _Vortex;
uniform 	mediump float _RotationSpeed;
uniform 	mediump float _Rotation;
uniform 	mediump float DetailDistance;
uniform 	mediump float _SceneIntersectionSoftness;
uniform 	mediump float _3DNoiseScale;
uniform 	mediump float _RayStep;
uniform 	mediump float gain;
uniform 	mediump float threshold;
uniform 	mediump float FadeDistance;
UNITY_LOCATION(0) uniform highp sampler2D _CameraDepthTexture;
UNITY_LOCATION(1) uniform mediump sampler3D _NoiseVolume;
in highp vec4 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
in highp vec3 vs_TEXCOORD3;
in highp vec3 vs_TEXCOORD4;
layout(location = 0) out highp vec4 SV_Target0;
vec3 u_xlat0;
vec4 u_xlat1;
vec3 u_xlat2;
vec3 u_xlat3;
vec3 u_xlat4;
mediump vec3 u_xlat16_5;
bool u_xlatb6;
vec3 u_xlat7;
vec3 u_xlat8;
mediump vec3 u_xlat16_9;
mediump vec3 u_xlat16_10;
vec4 u_xlat11;
vec4 u_xlat12;
bool u_xlatb12;
mediump float u_xlat16_13;
vec3 u_xlat14;
bool u_xlatb14;
mediump vec3 u_xlat16_15;
float u_xlat16;
vec4 u_xlat17;
bool u_xlatb17;
vec3 u_xlat18;
vec3 u_xlat19;
mediump vec3 u_xlat16_20;
vec2 u_xlat22;
bool u_xlatb22;
vec3 u_xlat23;
vec3 u_xlat24;
bool u_xlatb27;
vec3 u_xlat33;
mediump float u_xlat16_33;
mediump float u_xlat16_34;
vec3 u_xlat35;
mediump float u_xlat16_35;
bool u_xlatb35;
mediump float u_xlat16_36;
vec3 u_xlat37;
mediump float u_xlat16_37;
int u_xlati37;
float u_xlat38;
bool u_xlatb38;
float u_xlat43;
mediump float u_xlat16_47;
bvec2 u_xlatb48;
mediump float u_xlat16_56;
bool u_xlatb56;
mediump float u_xlat16_57;
vec2 u_xlat58;
mediump float u_xlat16_58;
bool u_xlatb58;
float u_xlat63;
bool u_xlatb63;
float u_xlat67;
mediump float u_xlat16_68;
int u_xlati69;
int u_xlati70;
float u_xlat71;
mediump float u_xlat16_72;
float u_xlat73;
mediump float u_xlat16_73;
int u_xlati74;
bool u_xlatb74;
float u_xlat75;
mediump float u_xlat16_77;
bool u_xlatb77;
mediump float u_xlat16_78;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD2.xyz + (-vs_TEXCOORD4.xyz);
    u_xlat63 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat63 = inversesqrt(u_xlat63);
    u_xlat0.xyz = vec3(u_xlat63) * u_xlat0.xyz;
    u_xlat1.xyz = vec3(1.0, 1.0, 1.0) / u_xlat0.xyz;
    u_xlat2.xyz = (-vs_TEXCOORD4.xyz) + _BoxMin.xyz;
    u_xlat2.xyz = u_xlat1.xyz * u_xlat2.xyz;
    u_xlat3.xyz = (-vs_TEXCOORD4.xyz) + _BoxMax.xyz;
    u_xlat1.xyz = u_xlat1.xyz * u_xlat3.xyz;
    u_xlat3.xyz = min(u_xlat2.xyz, u_xlat1.xyz);
    u_xlat1.xyz = max(u_xlat2.xyz, u_xlat1.xyz);
    u_xlat2.xy = max(u_xlat3.yz, u_xlat3.xx);
    u_xlat63 = max(u_xlat2.y, u_xlat2.x);
    u_xlat1.xy = min(u_xlat1.yz, u_xlat1.xx);
    u_xlat1.x = min(u_xlat1.y, u_xlat1.x);
#ifdef UNITY_ADRENO_ES3
    u_xlatb22 = !!(u_xlat1.x<u_xlat63);
#else
    u_xlatb22 = u_xlat1.x<u_xlat63;
#endif
    if(((int(u_xlatb22) * int(0xffffffffu)))!=0){discard;}
#ifdef UNITY_ADRENO_ES3
    u_xlatb22 = !!(u_xlat63<0.0);
#else
    u_xlatb22 = u_xlat63<0.0;
#endif
    u_xlat63 = (u_xlatb22) ? 0.0 : u_xlat63;
    u_xlat22.xy = vs_TEXCOORD1.xy / vs_TEXCOORD1.ww;
    u_xlat22.x = texture(_CameraDepthTexture, u_xlat22.xy).x;
    u_xlat22.x = _ZBufferParams.z * u_xlat22.x + _ZBufferParams.w;
    u_xlat22.x = float(1.0) / u_xlat22.x;
    u_xlat43 = dot(vs_TEXCOORD3.xyz, vs_TEXCOORD3.xyz);
    u_xlat2.x = inversesqrt(u_xlat43);
    u_xlat2.x = u_xlat2.x * vs_TEXCOORD3.z;
    u_xlat2.x = u_xlat22.x / u_xlat2.x;
    u_xlat23.xyz = u_xlat0.xyz * vec3(u_xlat63) + vs_TEXCOORD4.xyz;
    u_xlat0.xyz = u_xlat0.xyz * u_xlat1.xxx + vs_TEXCOORD4.xyz;
    u_xlat0.xyz = (-u_xlat23.xyz) + u_xlat0.xyz;
    u_xlat63 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat3.x = sqrt(u_xlat63);
    u_xlat24.xyz = _Time.xxx * Speed.xyz;
    u_xlat63 = inversesqrt(u_xlat63);
    u_xlat0.xyz = vec3(u_xlat63) * u_xlat0.xyz;
    u_xlat4.xyz = Stretch.xyz * vec3(vec3(_3DNoiseScale, _3DNoiseScale, _3DNoiseScale));
#ifdef UNITY_ADRENO_ES3
    u_xlatb63 = !!(Collisions!=0.0);
#else
    u_xlatb63 = Collisions!=0.0;
#endif
    u_xlat67 = (-_BoxMin.y) + _BoxMax.y;
    u_xlat16_5.xy = (-_VerticalGradientParams.xz) + _VerticalGradientParams.yw;
#ifdef UNITY_ADRENO_ES3
    u_xlatb6 = !!(_PrimitiveCutout<1.0);
#else
    u_xlatb6 = _PrimitiveCutout<1.0;
#endif
#ifdef UNITY_ADRENO_ES3
    u_xlatb27 = !!(0.00999999978<Coverage);
#else
    u_xlatb27 = 0.00999999978<Coverage;
#endif
    u_xlat16_47 = max(threshold, 0.0);
    u_xlat16_68 = _NoiseDetailRange + 1.0;
    u_xlat7.xyz = u_xlat24.xyz * vec3(_DetailRelativeSpeed);
    u_xlat8.xyz = u_xlat24.xyz * vec3(vec3(_BaseRelativeSpeed, _BaseRelativeSpeed, _BaseRelativeSpeed));
    u_xlat16_9.xyz = _AmbientColor.xyz * vec3(0.99999994, 0.99999994, 0.99999994);
    u_xlat16_72 = (-_AmbientHeightAbsorption.x) + _AmbientHeightAbsorption.y;
    u_xlatb48.xy = equal(_AmbientHeightAbsorption.xyxy, vec4(-1.0, -1.0, -1.0, -1.0)).xy;
    u_xlatb48.x = u_xlatb48.y && u_xlatb48.x;
    u_xlati69 = _ProxyVolume;
    u_xlati70 = Octaves;
    u_xlat71 = u_xlat3.x;
    u_xlat1.x = float(0.0);
    u_xlat1.y = float(0.0);
    u_xlat1.z = float(0.0);
    u_xlat1.w = float(0.0);
    u_xlat16_10.x = float(0.0);
    u_xlat16_10.y = float(0.0);
    u_xlat16_10.z = float(0.0);
    u_xlat16_73 = float(1.0);
    u_xlat11.y = _RayStep;
    u_xlat11.x = float(0.0);
    u_xlat11.z = float(1.40129846e-45);
    while(true){
#ifdef UNITY_ADRENO_ES3
        u_xlatb74 = !!(floatBitsToInt(u_xlat11.z)<STEP_COUNT);
#else
        u_xlatb74 = floatBitsToInt(u_xlat11.z)<STEP_COUNT;
#endif
#ifdef UNITY_ADRENO_ES3
        u_xlatb12 = !!(0.0<u_xlat71);
#else
        u_xlatb12 = 0.0<u_xlat71;
#endif
        u_xlatb74 = u_xlatb74 && u_xlatb12;
        if(!u_xlatb74){break;}
        u_xlati74 = floatBitsToInt(u_xlat11.z) * floatBitsToInt(u_xlat11.z);
        u_xlati74 = floatBitsToInt(u_xlat11.z) * u_xlati74;
        u_xlat16_13 = float(u_xlati74);
        u_xlat16_13 = u_xlat16_13 * _OptimizationFactor + 1.0;
        u_xlat11.w = u_xlat11.y * u_xlat16_13;
        u_xlat12.xyz = u_xlat0.xyz * u_xlat11.xxx + u_xlat23.xyz;
        u_xlat14.xyz = u_xlat12.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
        u_xlat14.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * u_xlat12.xxx + u_xlat14.xyz;
        u_xlat14.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * u_xlat12.zzz + u_xlat14.xyz;
        u_xlat14.xyz = u_xlat14.xyz + _VolumePosition.xyz;
        u_xlat14.xyz = u_xlat14.xyz + (-_WorldSpaceCameraPos.xyz);
        u_xlat75 = dot(u_xlat14.xyz, u_xlat14.xyz);
        u_xlat75 = sqrt(u_xlat75);
        u_xlat16_34 = u_xlat75 / FadeDistance;
#ifdef UNITY_ADRENO_ES3
        u_xlat16_34 = min(max(u_xlat16_34, 0.0), 1.0);
#else
        u_xlat16_34 = clamp(u_xlat16_34, 0.0, 1.0);
#endif
        u_xlat16_15.x = (-u_xlat16_34) + 1.0;
#ifdef UNITY_ADRENO_ES3
        u_xlatb14 = !!(u_xlat16_15.x<0.00100000005);
#else
        u_xlatb14 = u_xlat16_15.x<0.00100000005;
#endif
        if(u_xlatb14){
            break;
        }
        if(u_xlatb63){
            u_xlat14.xyz = u_xlat12.xyz + (-vs_TEXCOORD4.xyz);
            u_xlat14.x = dot(u_xlat14.xyz, u_xlat14.xyz);
            u_xlat14.x = sqrt(u_xlat14.x);
            u_xlat14.x = abs(u_xlat2.x) + (-u_xlat14.x);
            u_xlat73 = u_xlat14.x * _SceneIntersectionSoftness;
#ifdef UNITY_ADRENO_ES3
            u_xlat73 = min(max(u_xlat73, 0.0), 1.0);
#else
            u_xlat73 = clamp(u_xlat73, 0.0, 1.0);
#endif
#ifdef UNITY_ADRENO_ES3
            u_xlatb14 = !!(u_xlat73<0.00999999978);
#else
            u_xlatb14 = u_xlat73<0.00999999978;
#endif
            if(u_xlatb14){
                break;
            }
            u_xlat16_73 = u_xlat73;
        }
        u_xlat14.x = u_xlat12.y / u_xlat67;
        u_xlat14.x = u_xlat14.x + 0.5;
        u_xlat16_36 = u_xlat16_15.x * gain;
        u_xlat16_36 = u_xlat16_73 * u_xlat16_36;
        u_xlat35.xy = u_xlat14.xx + (-_VerticalGradientParams.xz);
        u_xlat35.xy = u_xlat35.xy / u_xlat16_5.xy;
#ifdef UNITY_ADRENO_ES3
        u_xlat35.xy = min(max(u_xlat35.xy, 0.0), 1.0);
#else
        u_xlat35.xy = clamp(u_xlat35.xy, 0.0, 1.0);
#endif
        u_xlat16_57 = u_xlat35.y * u_xlat35.x;
        u_xlat16_36 = u_xlat16_57 * u_xlat16_36;
#ifdef UNITY_ADRENO_ES3
        u_xlatb35 = !!(0.0<u_xlat16_73);
#else
        u_xlatb35 = 0.0<u_xlat16_73;
#endif
#ifdef UNITY_ADRENO_ES3
        u_xlatb56 = !!(0.0<u_xlat16_36);
#else
        u_xlatb56 = 0.0<u_xlat16_36;
#endif
        u_xlatb35 = u_xlatb56 && u_xlatb35;
        u_xlatb35 = u_xlatb6 && u_xlatb35;
        if(u_xlatb35){
            u_xlat12.xyz = u_xlat4.xyz * u_xlat12.xyz;
            u_xlat16_57 = u_xlat75 / DetailDistance;
#ifdef UNITY_ADRENO_ES3
            u_xlat16_57 = min(max(u_xlat16_57, 0.0), 1.0);
#else
            u_xlat16_57 = clamp(u_xlat16_57, 0.0, 1.0);
#endif
            u_xlat16_57 = (-u_xlat16_57) + 1.0;
            u_xlat75 = dot(u_xlat12.xyz, u_xlat12.xyz);
            u_xlat75 = sqrt(u_xlat75);
            u_xlat75 = u_xlat75 * _Vortex + _Rotation;
            u_xlat75 = _RotationSpeed * _Time.x + u_xlat75;
            u_xlat16 = sin(u_xlat75);
            u_xlat17.x = cos(u_xlat75);
            u_xlat18.x = sin((-u_xlat75));
            u_xlat18.y = u_xlat17.x;
            u_xlat17.x = dot(u_xlat12.zy, u_xlat18.xy);
            u_xlat18.z = u_xlat16;
            u_xlat17.y = dot(u_xlat12.zy, u_xlat18.yz);
            if(u_xlatb27){
                u_xlat17.z = u_xlat12.x;
                u_xlat33.xyz = u_xlat17.zxy * vec3(BaseTiling);
                u_xlat35.xyz = u_xlat33.xyz;
                u_xlat16 = float(0.0);
                for(int u_xlati_loop_1 = int(0) ; u_xlati_loop_1<u_xlati70 ; u_xlati_loop_1++)
                {
                    u_xlat58.x = float(u_xlati_loop_1);
                    u_xlat58.xy = u_xlat58.xx * vec2(0.0599999987, 0.150000006) + vec2(1.0, 1.0);
                    u_xlat18.xyz = u_xlat35.xyz / u_xlat58.xxx;
                    u_xlat35.xyz = u_xlat8.xyz * u_xlat58.yyy + u_xlat18.xyz;
                    u_xlat16_58 = textureLod(_NoiseVolume, u_xlat35.xyz, 0.0).x;
                    u_xlat16 = u_xlat16_58 + u_xlat16;
                }
                u_xlat33.x = float(u_xlati70);
                u_xlat33.x = u_xlat16 / u_xlat33.x;
                u_xlat16_33 = u_xlat33.x;
            } else {
                u_xlat16_33 = 0.0;
            }
            u_xlat16_36 = u_xlat16_36 * Coverage;
            u_xlat16_35 = u_xlat16_33 * u_xlat16_36;
            u_xlat16_56 = u_xlat16_33 * u_xlat16_36 + -0.5;
            u_xlat16_56 = u_xlat16_56 * u_xlat16_47 + 0.5;
#ifdef UNITY_ADRENO_ES3
            u_xlat16_56 = min(max(u_xlat16_56, 0.0), 1.0);
#else
            u_xlat16_56 = clamp(u_xlat16_56, 0.0, 1.0);
#endif
            u_xlat35.x = u_xlat16_35 * u_xlat16_56;
#ifdef UNITY_ADRENO_ES3
            u_xlat35.x = min(max(u_xlat35.x, 0.0), 1.0);
#else
            u_xlat35.x = clamp(u_xlat35.x, 0.0, 1.0);
#endif
            u_xlat16_36 = (-u_xlat35.x) * _DetailMaskingThreshold + 1.0;
#ifdef UNITY_ADRENO_ES3
            u_xlat16_36 = min(max(u_xlat16_36, 0.0), 1.0);
#else
            u_xlat16_36 = clamp(u_xlat16_36, 0.0, 1.0);
#endif
#ifdef UNITY_ADRENO_ES3
            u_xlatb56 = !!(0.0<u_xlat16_57);
#else
            u_xlatb56 = 0.0<u_xlat16_57;
#endif
#ifdef UNITY_ADRENO_ES3
            u_xlatb77 = !!(0.0<u_xlat35.x);
#else
            u_xlatb77 = 0.0<u_xlat35.x;
#endif
            u_xlatb56 = u_xlatb77 && u_xlatb56;
#ifdef UNITY_ADRENO_ES3
            u_xlatb77 = !!(0.0<u_xlat16_36);
#else
            u_xlatb77 = 0.0<u_xlat16_36;
#endif
            u_xlatb56 = u_xlatb77 && u_xlatb56;
            if(u_xlatb56){
                u_xlat16_78 = u_xlat16_57 * u_xlat16_36;
                u_xlat17.w = u_xlat12.x;
                u_xlat37.xyz = u_xlat17.wxy * vec3(DetailTiling) + u_xlat7.xyz;
                u_xlat16_56 = textureLod(_NoiseVolume, u_xlat37.xyz, 0.0).x;
                u_xlat16_56 = u_xlat16_56 * u_xlat16_78;
#ifdef UNITY_ADRENO_ES3
                u_xlatb77 = !!(1<u_xlati70);
#else
                u_xlatb77 = 1<u_xlati70;
#endif
                if(u_xlatb77){
                    u_xlat37.xyz = u_xlat17.wxy * vec3(0.5, 0.5, 0.5);
                    u_xlat37.xyz = u_xlat37.xyz * vec3(DetailTiling) + vec3(0.5, 0.5, 0.5);
                    u_xlat16_78 = u_xlat16_56 * _Curl;
                    u_xlat16_78 = dot(vec2(u_xlat16_78), vec2(u_xlat16_36));
                    u_xlat37.xyz = vec3(u_xlat16_78) + u_xlat37.xyz;
                    u_xlat37.xyz = u_xlat24.xyz * vec3(_DetailRelativeSpeed) + u_xlat37.xyz;
                    u_xlat37.xyz = u_xlat37.xyz + vec3(-1.0, -1.0, -1.0);
                    u_xlat16_77 = textureLod(_NoiseVolume, u_xlat37.xyz, 0.0).x;
                    u_xlat16_77 = u_xlat16_77 * u_xlat16_57;
                    u_xlat16_77 = u_xlat16_77 * 1.5;
                    u_xlat16_77 = u_xlat16_77 * u_xlat16_36 + u_xlat16_56;
                    u_xlat16_77 = u_xlat16_77;
                } else {
                    u_xlat16_77 = u_xlat16_56;
                }
                u_xlat16_37 = u_xlat16_77 * 0.5;
#ifdef UNITY_ADRENO_ES3
                u_xlat16_37 = min(max(u_xlat16_37, 0.0), 1.0);
#else
                u_xlat16_37 = clamp(u_xlat16_37, 0.0, 1.0);
#endif
                u_xlat16_37 = u_xlat16_37;
            } else {
                u_xlat16_37 = 0.0;
            }
            u_xlat17.x = (-u_xlat16_37) * _NoiseDetailRange + u_xlat35.x;
            u_xlat17.x = u_xlat16_68 * u_xlat17.x;
            u_xlat17.x = u_xlat17.x * NoiseDensity;
#ifdef UNITY_ADRENO_ES3
            u_xlat17.x = min(max(u_xlat17.x, 0.0), 1.0);
#else
            u_xlat17.x = clamp(u_xlat17.x, 0.0, 1.0);
#endif
#ifdef UNITY_ADRENO_ES3
            u_xlatb38 = !!(0.0<u_xlat17.x);
#else
            u_xlatb38 = 0.0<u_xlat17.x;
#endif
            u_xlat17.x = u_xlat16_15.x * u_xlat17.x;
            u_xlat17.x = u_xlatb38 ? u_xlat17.x : float(0.0);
        } else {
            u_xlat17.x = 0.0;
        }
#ifdef UNITY_ADRENO_ES3
        u_xlatb38 = !!(u_xlati69==1);
#else
        u_xlatb38 = u_xlati69==1;
#endif
        if(u_xlatb38){
            u_xlat16_15.xyz = u_xlat16_9.xyz;
        } else {
            u_xlat16_15.xyz = _AmbientColor.xyz;
        }
        u_xlat38 = u_xlat14.x + (-_AmbientHeightAbsorption.x);
        u_xlat38 = u_xlat38 / u_xlat16_72;
#ifdef UNITY_ADRENO_ES3
        u_xlat38 = min(max(u_xlat38, 0.0), 1.0);
#else
        u_xlat38 = clamp(u_xlat38, 0.0, 1.0);
#endif
        u_xlat16_78 = u_xlat38 * -1.44269502;
        u_xlat16_78 = exp2(u_xlat16_78);
        u_xlat16_78 = (-u_xlat16_78) + 1.0;
        u_xlat16_78 = (u_xlatb48.x) ? 1.0 : u_xlat16_78;
        u_xlat38 = u_xlat16_73 * u_xlat17.x;
        u_xlat18.xyz = u_xlat16_15.xyz * vec3(u_xlat38);
        u_xlat19.xyz = u_xlat16_10.xyz * vec3(u_xlat38);
        u_xlat16_10.xyz = vec3(u_xlat16_78) * u_xlat19.xyz;
        u_xlat16_20.xyz = u_xlat18.xyz * vec3(u_xlat16_78) + u_xlat16_10.xyz;
        u_xlat18.xyz = u_xlat16_20.xyz * _Color.xyz;
        u_xlat17.xzw = u_xlat17.xxx * u_xlat16_15.xyz;
        u_xlat12.xyz = u_xlat18.xyz * vec3(vec3(_LightExposure, _LightExposure, _LightExposure)) + u_xlat17.xzw;
        u_xlat12.w = u_xlat38 * _PushAlpha;
        u_xlat17.x = (-u_xlat1.w) + 1.0;
        u_xlat12 = u_xlat12 * u_xlat17.xxxx + u_xlat1;
#ifdef UNITY_ADRENO_ES3
        u_xlatb17 = !!(0.999000013<u_xlat12.w);
#else
        u_xlatb17 = 0.999000013<u_xlat12.w;
#endif
        if(u_xlatb17){
            u_xlat1 = u_xlat12;
            break;
        }
        u_xlat1 = u_xlat12;
        u_xlat11.z = intBitsToFloat(floatBitsToInt(u_xlat11.z) + 1);
        u_xlat11.x = u_xlat11.y * u_xlat16_13 + u_xlat11.x;
        u_xlat71 = (-u_xlat11.y) * u_xlat16_13 + u_xlat71;
        u_xlat11.xyz = u_xlat11.xwz;
    }
    u_xlat0.xyz = log2(u_xlat1.xyz);
    u_xlat0.xyz = u_xlat0.xyz * vec3(0.454545468, 0.454545468, 0.454545468);
    u_xlat0.xyz = exp2(u_xlat0.xyz);
    SV_Target0.w = u_xlat1.w;
#ifdef UNITY_ADRENO_ES3
    SV_Target0.w = min(max(SV_Target0.w, 0.0), 1.0);
#else
    SV_Target0.w = clamp(SV_Target0.w, 0.0, 1.0);
#endif
    u_xlat16_5.xyz = min(u_xlat0.xyz, vec3(65504.0, 65504.0, 65504.0));
    SV_Target0.xyz = u_xlat16_5.xyz;
    return;
}

#endif
                               $Globalsx  (      _Time                            _WorldSpaceCameraPos                        _ZBufferParams                        
   Collisions                    p      _AmbientHeightAbsorption                  �      _VerticalGradientParams                   �   
   STEP_COUNT                   �      _ProxyVolume                 �      Octaves                  �      _Color                    �      _BoxMin                   �      _BoxMax                   �      Stretch                   �      _AmbientColor                     �      _VolumePosition                         Speed                          DetailTiling                       _PrimitiveCutout                        _LightExposure                    $  
   _PushAlpha                    (     _DetailMaskingThreshold                   ,     _OptimizationFactor                   0     _BaseRelativeSpeed                    4     _DetailRelativeSpeed                  8     _NoiseDetailRange                     <     _Curl                     @  
   BaseTiling                    D     Coverage                  H     NoiseDensity                  L     _Vortex                   P     _RotationSpeed                    T  	   _Rotation                     X     DetailDistance                    \     _SceneIntersectionSoftness                    `     _3DNoiseScale                     d     _RayStep                  h     gain                  l  	   threshold                     p     FadeDistance                  t     unity_ObjectToWorld                  0          $Globalsl        _WorldSpaceCameraPos                         _ProjectionParams                           L                     `     unity_ObjectToWorld                         unity_WorldToObject                  `      unity_MatrixV                    �      unity_MatrixInvV                 �      unity_MatrixVP                                _CameraDepthTexture                   _NoiseVolume             