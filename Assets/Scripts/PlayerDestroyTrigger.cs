﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDestroyTrigger : MonoBehaviour
{
    [SerializeField] private LevelManager _levelManager;

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            _levelManager.GameOver();
        }
        else if (other.CompareTag("Bot"))
        {
            Destroy(other.gameObject);
        }
    }
}
