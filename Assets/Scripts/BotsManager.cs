﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotsManager : MonoBehaviour
{
    [SerializeField] private int _botsNumber;
    [SerializeField] private Bot _botPrefab;
    [SerializeField] private LevelCreateManager _levelCreateManager;
    [SerializeField] private Vector2 _botCreateDelay;
    [SerializeField] private float _botCreateY;
    [SerializeField] private float _botCreateRndOffset;

    private List<Bot> _bots;
    private Coroutine _createCoroutine;

    public void StartGame()
    {
        Clear();

        _bots = new List<Bot>();

        if (_createCoroutine != null) StopCoroutine(_createCoroutine);
        _createCoroutine = StartCoroutine(CreateBots());
    }

    private IEnumerator CreateBots()
    {
        var firstPlatform = _levelCreateManager.GetFirstPlatform();

        
        for (int i = 0; i < _botsNumber; i++)
        {
            if (firstPlatform == null) yield break;
            
            var bot = Instantiate(_botPrefab);

            bot.transform.position = firstPlatform.transform.position + Vector3.up * _botCreateY
                + Quaternion.Euler(0, Random.Range(0, 360), 0) * Vector3.forward * _botCreateRndOffset;

            bot.transform.forward = firstPlatform.transform.forward;

            bot.StartGame(_levelCreateManager);

            yield return new WaitForSeconds(Random.Range(_botCreateDelay.x, _botCreateDelay.y));
        }
    }

    public void Clear()
    {
        if (_bots != null)
        {
            foreach (var p in _bots)
            {
                if (p != null && p.gameObject != null) Destroy(p.gameObject);
            }
        }
    }
}
