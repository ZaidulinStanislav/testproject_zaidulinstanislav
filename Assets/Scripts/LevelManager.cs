﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum GameState
{
    None,
    MainMenu,
    LevelComplete,
    LevelFailed,
    Game
}

public class LevelManager : MonoBehaviour
{
    [Header("General")]
    [SerializeField] private LevelCreateManager _levelCreateManager;
    [SerializeField] private Player _player;
    [SerializeField] private ProgressBarManager _progressBarManager;
    [SerializeField] private BotsManager _botsManager;

    [Header("Effects")]
    [SerializeField] private Transform _waterTransform;
    [SerializeField] private ParticleSystem[] _completeParticles;
    [SerializeField] private Animator _longJumpTextAnimator;
    [SerializeField] private Animator _perfectTextAnimator;
    [SerializeField] private Animator _stopTextAnimator;

    [Header("Long jump")]
    [SerializeField] private int _longJumpPlatformNumber;
    

    [Header("UI")]
    [SerializeField] private Animator _uiAnimator;
    [SerializeField] private Button _startGameButton;
    [SerializeField] private Button _restartButton;
    [SerializeField] private Button _nextLevelButton;
    [SerializeField] private CinemachineVirtualCamera _mainMenuCamera;
    [SerializeField] private CinemachineVirtualCamera _gameVirtualCamera;
    [SerializeField] private TextMeshProUGUI _mainMenuLevelNumber;

    public int LevelNumber { get; private set; }

    private GameState _gameState;
    private Storage _storage;
    private int _previousCompletedPlatformId;

    private void Awake()
    {
        _startGameButton.onClick.AddListener(StartGame);
        _restartButton.onClick.AddListener(ToMainMenu);
        _nextLevelButton.onClick.AddListener(ToMainMenu);

        _storage = Storage.instance;

        _gameState = GameState.None;
    }

    private void Start()
    {
        Application.targetFrameRate = 60;

        if (LevelNumber == -23) Handheld.Vibrate();

        ToMainMenu();
    }

    public void StartGame()
    {
        if (_gameState == GameState.Game) return;
        _gameState = GameState.Game;

        _mainMenuCamera.enabled = false;
        _gameVirtualCamera.enabled = true;

        _startGameButton.enabled = false;
        _restartButton.enabled = true;

        _player.StartGame();

        _progressBarManager.Initialize(LevelNumber);

        _botsManager.StartGame();

        _uiAnimator.SetTrigger("ToGame");
    }

    public void ToMainMenu()
    {
        if (_gameState == GameState.MainMenu) return;
        _gameState = GameState.MainMenu;

        _mainMenuCamera.enabled = true;
        _gameVirtualCamera.enabled = false;

        _startGameButton.enabled = true;
        _restartButton.enabled = false;

        _uiAnimator.SetTrigger("ToMainMenu");

        LevelNumber = Storage.instance.LevelNumber;

        Vector3 firstPlatformPosition;
        Vector3 firstPlatformDirection;
        _levelCreateManager.Initialize(LevelNumber, out firstPlatformPosition, out firstPlatformDirection);

        _player.Initialize(firstPlatformPosition, firstPlatformDirection);

        _mainMenuLevelNumber.text = LevelNumber.ToString();

        _previousCompletedPlatformId = _levelCreateManager.LevelPlatformNumber;
    }

    public void CompleteLevel()
    {
        if (_gameState == GameState.LevelComplete) return;
        _gameState = GameState.LevelComplete;

        _storage.LevelNumber = LevelNumber + 1;

        _botsManager.Clear();

        _player.LevelComplete();

        _uiAnimator.SetTrigger("ToLevelComplete");

        Debug.Log("Level complete!");
    }

    public void GameOver()
    {
        if (_gameState == GameState.LevelFailed) return;
        _gameState = GameState.LevelFailed;

        _player.Death();

        //Play sink particles
        /*
        _sinkParticles.gameObject.SetActive(true);
        _sinkParticles.transform.position = new Vector3(_player.transform.position.x,
            _waterTransform.TransformPoint(Vector3.up / 2.0f).y,
            _player.transform.position.z);
            */
        _botsManager.Clear();

        _uiAnimator.SetTrigger("ToLevelFailed");
    }

    public void CompletePlatform(int platformId, bool isPerfect = false)
    {
        if (_previousCompletedPlatformId - platformId > _longJumpPlatformNumber)
        {
            _longJumpTextAnimator.SetTrigger("Appear");
        }
        else if (platformId > _previousCompletedPlatformId)
        {
            _stopTextAnimator.SetTrigger("Appear");
        }
        else if (isPerfect)
        {
            _perfectTextAnimator.SetTrigger("Appear");
        }
        _previousCompletedPlatformId = platformId;// (platformId < _previousCompletedPlatformId) ? platformId : _previousCompletedPlatformId;

        _progressBarManager.UpdateValue(((float)(_levelCreateManager.LevelPlatformNumber - _previousCompletedPlatformId)
            / (float)_levelCreateManager.LevelPlatformNumber));
    }
}
