﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.NiceVibrations;

public class Player : MonoBehaviour
{
    [Header("General")]
    [SerializeField] private Rigidbody _body;
    [SerializeField] private Camera _camera;
    [SerializeField] private PlayerShadow _playerShadow;
    [SerializeField] private float _playerInitialPositionOffsetY;
    [SerializeField] private Animator _animator;
    [SerializeField] private LevelManager _levelManager;

    [Header("Moving properties")]
    [SerializeField] private float _movingSpeed;
    [SerializeField] private float _rotationSpeed;
    [SerializeField] private float _maxRotationAngle;

    [Header("Jump properties")]
    [SerializeField] private float _fragilePlatformJumpPower;
    [SerializeField] private float _gorundPlatformJumpPower;
    [SerializeField] private float _trampolinePlatformJumpPower;
    [SerializeField] private float _movingPlatformJumpPower;
    [SerializeField] private int _jumpAnimationNumber;

    private bool _isJumpEnabled;
    private float _delayBeforeJumps = 0.5f;
    private Vector2 _controlsCenterUI;
    private Vector3 _movingDirection;
    private Vector3 _targetMovingDirection;
    private Vector3 _startMovingDirection;
    private Vector3 _startMousePosition;
    private float _cameraWidth;
    private bool _isMovingForward;
    private bool _isActive;

    private int _previousCompletedPlatformId = -1;

    private void Awake()
    {
        _cameraWidth = _camera.pixelWidth;

        _movingDirection = Vector3.forward;
    }

    public void Initialize(Vector3 initialPosition, Vector3 initialDirection)
    {
        transform.position = initialPosition + Vector3.up * _playerInitialPositionOffsetY;
        transform.forward = new Vector3(initialDirection.x, 0, initialDirection.z);

        _isJumpEnabled = true;
        _isActive = false;
        //_body.isKinematic = false;
        _body.useGravity = true;
    }

    public void StartGame()
    {
        _isActive = true;
    }

    private void Update()
    {
        if (!_isActive) return;

        if(Input.GetMouseButtonDown(0))
        {
            _startMousePosition = Input.mousePosition;

            _startMovingDirection = transform.forward;

            _isMovingForward = true;
        }
        else if(Input.GetMouseButtonUp(0))
        {
            _targetMovingDirection = transform.forward;

            _isMovingForward = false;

            _body.velocity = new Vector3(0, _body.velocity.y, 0);
        }
        else if (Input.GetMouseButton(0))
        {
            var mousePosition = Input.mousePosition;

            var offset = (mousePosition.x - _startMousePosition.x) / _cameraWidth;
            var rotateAngle = offset * _maxRotationAngle;

            _targetMovingDirection = Quaternion.Euler(0, rotateAngle, 0) * _startMovingDirection;
        }
    }

    private void FixedUpdate()
    {
        if (!_isActive) return;

        transform.forward = Vector3.Lerp(transform.forward, _targetMovingDirection, _rotationSpeed);

        if (_isMovingForward) _body.velocity = new Vector3(transform.forward.x * _movingSpeed, _body.velocity.y, transform.forward.z * _movingSpeed);

    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.CompareTag("Platform"))
        {
            if (_isJumpEnabled)
            {
                var platform = collision.collider.GetComponent<Platform>();

                if(platform == null) platform = collision.collider.GetComponentInParent<Platform>();

                _body.velocity = Vector3.zero;

                MMVibrationManager.Haptic(HapticTypes.MediumImpact);

                switch (platform.PlatformType)
                {
                    case PlatformType.Fragile:

                        _body.AddForce(Vector3.up * _fragilePlatformJumpPower, ForceMode.Impulse);

                        var fragilePlatform = platform.GetComponent<FragilePlatform>();
                        var platformDestroyed = fragilePlatform.DecreaseHealth();
                        if (platformDestroyed)
                        {
                            _playerShadow.ForceDeactivate();
                        }
                        break;
                    case PlatformType.Moving:
                        _body.AddForce(Vector3.up * _movingPlatformJumpPower, ForceMode.Impulse);
                        break;
                    case PlatformType.Ground:
                        _body.AddForce(Vector3.up * _gorundPlatformJumpPower, ForceMode.Impulse);
                        break;
                    case PlatformType.Trampoline:
                        _body.AddForce(Vector3.up * _trampolinePlatformJumpPower, ForceMode.Impulse);
                        break;
                    default:
                        break;
                }

                bool isPerfect;
                platform.CompletePlatform(transform.position,out isPerfect);

                _levelManager.CompletePlatform(platform.PlatformId, isPerfect);

                _isJumpEnabled = true;

                _animator.SetTrigger("Jump" + Random.Range(0, _jumpAnimationNumber));

                Invoke("ActivateJump", _delayBeforeJumps);
            }
        }
    }

    private void ActivateJump()
    {
        _isJumpEnabled = true;
    }

    public void LevelComplete()
    {
        MMVibrationManager.Haptic(HapticTypes.Success);

        _isActive = false;

        _body.velocity = Vector3.zero;
        //_body.isKinematic = true;
        _body.useGravity = false;
    }

    public void Death()
    {
        MMVibrationManager.Haptic(HapticTypes.Failure);

        _isActive = false;

        _body.velocity = Vector3.zero;
        //_body.isKinematic = true;
        _body.useGravity = false;
    }
}
