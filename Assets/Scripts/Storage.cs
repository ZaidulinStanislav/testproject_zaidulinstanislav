﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[Serializable]
public class PlayerData
{
    public int LevelNumber;

    public int ActiveSkinId;

    public int ForTradeSkinId;

    public int PossibleRateNumber;

    public float SoundValue;

    public float MusicValue;

    public bool IsVibration;

    public int CoinNumber;

    public bool IsAds;

    public string PlayerName;

    public int SpeedLevel;
    public int CapacityLevel;
    public int HealthLevel;
}

public class Storage : MonoBehaviour
{
    private static Storage _instance;
    public static Storage instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<Storage>();
            }

            return _instance;
        }
    }

    public PlayerData InitialData;

    private int _coinNumber = -1;
    public int CoinNumber
    {
        set
        {
            _coinNumber = value;
            Save();
        }

        get
        {
            if (_coinNumber == -1)
            {
                LoadFromFile();
            }

            return _coinNumber;
        }

    }

    private int _speedLevel = -1;
    public int SpeedLevel
    {
        set
        {
            _speedLevel = value;
            Save();
        }

        get
        {
            if (_speedLevel == -1)
            {
                LoadFromFile();
            }

            return _speedLevel;
        }
    }
    private int _capacityLevel = -1;
    public int CapacityLevel
    {
        set
        {
            _capacityLevel = value;
            Save();
        }

        get
        {
            if (_capacityLevel == -1)
            {
                LoadFromFile();
            }

            return _capacityLevel;
        }
    }

    private int _healthLevel = -1;
    public int HealthLevel
    {
        set
        {
            _healthLevel = value;
            Save();
        }

        get
        {
            if (_healthLevel == -1)
            {
                LoadFromFile();
            }

            return _healthLevel;
        }
    }


    private int _levelNumber = -1;
    public int LevelNumber
    {
        set
        {
            _levelNumber = value;
            Save();
        }

        get
        {
            if (_levelNumber == -1)
            {
                LoadFromFile();
            }

            return _levelNumber;
        }
    }

    private string _playerName = "Player";
    public string PlayerName
    {
        set
        {
            _playerName = value;
            Save();
        }

        get
        {
            return _playerName;
        }
    }

    private int _activeSkinId = -1;
    public int ActiveSkinId
    {
        set
        {
            _activeSkinId = value;

            Save();
        }

        get
        {
            if (_activeSkinId == -1)
            {
                LoadFromFile();
            }

            return _activeSkinId;
        }
    }

    private int _forTradeSkinId = -1;
    public int ForTradeSkinId
    {
        set
        {
            _forTradeSkinId = value;

            Save();
        }

        get
        {
            if (_forTradeSkinId == -1)
            {
                LoadFromFile();
            }

            return _forTradeSkinId;
        }
    }

    private int _possibleRateNumber = 0;
    public int PossibleRateNumber
    {
        set
        {
            _possibleRateNumber = value;

            Save();
        }

        get
        {
            if (_possibleRateNumber == -1)
            {
                LoadFromFile();
            }

            return _possibleRateNumber;
        }
    }

    private float _soundValue = -1;
    public float SoundValue
    {
        set
        {
            _soundValue = value;

            Save();
        }

        get
        {
            if (_soundValue == -1)
            {
                LoadFromFile();
            }

            return _soundValue;
        }
    }

    private float _musicValue = -1;
    public float MusicValue
    {
        set
        {
            _musicValue = value;

            Save();
        }

        get
        {
            if (_musicValue == -1)
            {
                LoadFromFile();
            }

            return _musicValue;
        }
    }

    private bool _isVibration;
    public bool IsVibration
    {
        set
        {
            _isVibration = value;

            Save();
        }

        get
        {
            return _isVibration;
        }
    }

    private bool _isAds;
    public bool IsAds
    {
        set
        {
            _isAds = value;

            Save();
        }

        get
        {
            return _isAds;
        }
    }

    private bool _clearStorage = false;

    private void Awake()
    {

    }

    public void LoadFromFile()
    {
        var data = Load();

        _levelNumber = data.LevelNumber;

        _activeSkinId = data.ActiveSkinId;

        _forTradeSkinId = data.ForTradeSkinId;

        _possibleRateNumber = data.PossibleRateNumber;

        _soundValue = data.SoundValue;

        _musicValue = data.MusicValue;

        _isVibration = data.IsVibration;

        _coinNumber = data.CoinNumber;

        _isAds = data.IsAds;

        _playerName = data.PlayerName;

        _speedLevel = data.SpeedLevel;
        _capacityLevel = data.CapacityLevel;
        _healthLevel = data.HealthLevel;
    }

    private static string GetFilename()
    {
        return Application.persistentDataPath + "/" + "progress.dat";
    }

    public void Save()
    {
        Save(new PlayerData
        {
            ActiveSkinId = ActiveSkinId,
            LevelNumber = LevelNumber,
            ForTradeSkinId = ForTradeSkinId,
            PossibleRateNumber = PossibleRateNumber,
            MusicValue = MusicValue,
            SoundValue = SoundValue,
            IsVibration = IsVibration,
            CoinNumber = CoinNumber,
            IsAds = IsAds,
            PlayerName = PlayerName,
            CapacityLevel = CapacityLevel,
            HealthLevel = HealthLevel,
            SpeedLevel = SpeedLevel
        });
    }

    private void Save(PlayerData data)
    {
        var file = File.Open(GetFilename(), FileMode.OpenOrCreate);

        var bf = new BinaryFormatter();

        bf.Serialize(file, data);

        file.Close();
    }

    public PlayerData Load()
    {
        if (File.Exists(GetFilename()) && !_clearStorage)
        {

            using (var file = File.Open(GetFilename(), FileMode.Open))
            {
                var bf = new BinaryFormatter();

                return bf.Deserialize(file) as PlayerData;
            }
        }
        else
        {
            _clearStorage = false;

            var data = InitialData;

            Save(data);

            return data;
        }
    }

    public void ClearStorage()
    {
        _clearStorage = true;

        LoadFromFile();
    }

    private int screenNumber = 0;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            screenNumber++;
            ScreenCapture.CaptureScreenshot("screen" + screenNumber.ToString() + ".png");
        }
    }
}
