﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundPlatformCreateManager : MonoBehaviour
{
    //[SerializeField] private float _deltaWidth;
    //[SerializeField] private float _deltaLength;
    //[SerializeField] private float _createRandomnessRadius;
    //[SerializeField] private float _estimatedLengthToWidthRatio;
    //[SerializeField] private float _overextendLengthCoef;

    [SerializeField] private float _deltaAngle;
    [SerializeField] private float _deltaOffset;
    [SerializeField] private float _groundObjectY;
    [SerializeField] private float _createRandomnOffset;

    [SerializeField] private GroundPlatform _groundPlatformPrefab;

    private List<GroundPlatform> _groundPlatforms;

    public void Initialize(Vector3 centerPoint, float levelRadius)
    {
        Clear();

        centerPoint.y = _groundObjectY;

        _groundPlatforms = new List<GroundPlatform>();

        var angleOffsetNumber = 360 / _deltaAngle;
        var offsetNumber = levelRadius / _deltaOffset;

        for (int i = 0; i < angleOffsetNumber; i++)
        {
            var axis = Quaternion.Euler(0, i * _deltaAngle, 0) * Vector3.forward;

            for (int t = 1; t < offsetNumber; t++)
            {
                var platform = Instantiate(_groundPlatformPrefab, transform);
                platform.transform.position = centerPoint
                    + axis * t * _deltaOffset                   
                    + Quaternion.Euler(0, Random.Range(0, 360), 0) * Vector3.forward * Random.Range(_createRandomnOffset / 3.0f, _createRandomnOffset);

                _groundPlatforms.Add(platform);
            }
        }

        //var direction = (endPlatformProjection - firstPlatformProjection).normalized;
        //direction.y = 0;

        //var norm = Quaternion.Euler(0, 90, 0) * direction;

        //var center = (endPlatformProjection + firstPlatformProjection) / 2.0f;

        //var length = (endPlatformProjection - firstPlatformProjection).magnitude * _overextendLengthCoef;

        //var estimatedWidth = length * _estimatedLengthToWidthRatio;

        //var createWidthNumber = (int)Mathf.Ceil(((estimatedWidth / _deltaWidth) / 2.0f));

        //float widthDecreaseValue = length / createWidthNumber;

        ////create center line from first to end
        //var createLengthNumber = (int)Mathf.Ceil(((length / _deltaLength) / 2.0f));

        //var centerObj = Instantiate(_groundPlatformPrefab, transform);
        //centerObj.transform.position = new Vector3(center.x, _groundObjectY, center.z)
        //    + Quaternion.Euler(0, Random.Range(0, 360), 0) * Vector3.forward * Random.Range(_createRandomnessRadius / 3.0f, _createRandomnessRadius);

        //_groundPlatforms.Add(centerObj);

        //for (int i = 1; i < createLengthNumber; i++)
        //{
        //    var upObj = Instantiate(_groundPlatformPrefab, transform);
        //    var downObj = Instantiate(_groundPlatformPrefab, transform);

        //    upObj.transform.position = center + direction * i * _deltaLength
        //        + Quaternion.Euler(0, Random.Range(0, 360), 0) * Vector3.forward * Random.Range(_createRandomnessRadius / 3.0f, _createRandomnessRadius);
        //    downObj.transform.position = center - direction * i * _deltaLength
        //        + Quaternion.Euler(0, Random.Range(0, 360), 0) * Vector3.forward * Random.Range(_createRandomnessRadius / 3.0f, _createRandomnessRadius);

        //    _groundPlatforms.Add(upObj);
        //    _groundPlatforms.Add(downObj);
        //}

        //for (int i = 1; i < createWidthNumber; i++)
        //{
        //    var centerObjRight = Instantiate(_groundPlatformPrefab, transform);
        //    centerObjRight.transform.position = center + norm * i * _deltaWidth
        //        + Quaternion.Euler(0, Random.Range(0, 360), 0) * Vector3.forward * Random.Range(_createRandomnessRadius / 3.0f, _createRandomnessRadius);

        //    var centerObjLeft = Instantiate(_groundPlatformPrefab, transform);
        //    centerObjLeft.transform.position = center - norm * i * _deltaWidth
        //        + Quaternion.Euler(0, Random.Range(0, 360), 0) * Vector3.forward * Random.Range(_createRandomnessRadius / 3.0f, _createRandomnessRadius);

        //    _groundPlatforms.Add(centerObjRight);
        //    _groundPlatforms.Add(centerObjLeft);

        //    createLengthNumber = (int)Mathf.Ceil((((length - widthDecreaseValue * i) / _deltaLength) / 2.0f));

        //    for (int t = 1; t < createLengthNumber; t++)
        //    {
        //        var upObjRight = Instantiate(_groundPlatformPrefab, transform);
        //        var downObjRight = Instantiate(_groundPlatformPrefab, transform);

        //        upObjRight.transform.position = centerObjRight.transform.position + direction * t * _deltaLength
        //            + Quaternion.Euler(0, Random.Range(0, 360), 0) * Vector3.forward * Random.Range(_createRandomnessRadius / 3.0f, _createRandomnessRadius);
        //        downObjRight.transform.position = centerObjRight.transform.position - direction * t * _deltaLength
        //            + Quaternion.Euler(0, Random.Range(0, 360), 0) * Vector3.forward * Random.Range(_createRandomnessRadius / 3.0f, _createRandomnessRadius);

        //        _groundPlatforms.Add(upObjRight);
        //        _groundPlatforms.Add(downObjRight);

        //        var upObjLeft = Instantiate(_groundPlatformPrefab, transform);
        //        var downObjLeft = Instantiate(_groundPlatformPrefab, transform);

        //        upObjLeft.transform.position = centerObjLeft.transform.position + direction * t * _deltaLength
        //            + Quaternion.Euler(0, Random.Range(0, 360), 0) * Vector3.forward * Random.Range(_createRandomnessRadius / 3.0f, _createRandomnessRadius);
        //        downObjLeft.transform.position = centerObjLeft.transform.position - direction * t * _deltaLength
        //            + Quaternion.Euler(0, Random.Range(0, 360), 0) * Vector3.forward * Random.Range(_createRandomnessRadius / 3.0f, _createRandomnessRadius);

        //        _groundPlatforms.Add(upObjLeft);
        //        _groundPlatforms.Add(downObjLeft);
        //    }
        //}
    }

    private void Clear()
    {
        if (_groundPlatforms != null)
        {
            foreach (var p in _groundPlatforms)
            {
                if (p.gameObject != null) Destroy(p.gameObject);
            }
        }
    }
}
