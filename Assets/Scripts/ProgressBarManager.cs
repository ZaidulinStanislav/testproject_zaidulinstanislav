﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ProgressBarManager : MonoBehaviour
{
    [SerializeField] private Transform _scaledImage;
    [SerializeField] private TextMeshProUGUI _currentLevelText;
    [SerializeField] private TextMeshProUGUI _nextLevelText;

    public void Initialize(int levelNumber)
    {
        _scaledImage.localScale = new Vector3(0, 1, 1);

        _currentLevelText.text = levelNumber.ToString();
        _nextLevelText.text = (levelNumber + 1).ToString();
    }

    public void UpdateValue(float coef)
    {
        coef = Mathf.Clamp01(coef);
        _scaledImage.localScale = new Vector3(coef, 1, 1);
    }
}
