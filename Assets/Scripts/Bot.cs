﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bot : MonoBehaviour
{
    [Header("General")]
    [SerializeField] private Rigidbody _body;
    [SerializeField] private Animator _animator;

    [Header("Moving properties")]
    [SerializeField] private Vector2 _jumpPowerRangeZ;
    [SerializeField] private Vector2 _jumpPowerRangeY;
    [SerializeField] private float _jumpAngleMax;
    [SerializeField] private int _jumpAnimationNumber;

    [Header("Behaviour properties")]
    [SerializeField] private float _jumpUpProbability;

    private LevelCreateManager _levelCreateManager;

    public void StartGame(LevelCreateManager levelCreateManager)
    {
        _levelCreateManager = levelCreateManager;

        //_body.isKinematic = false;
        _body.useGravity = true;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Platform"))
        {
            var platform = collision.collider.GetComponent<Platform>();

            if (platform == null) platform = collision.collider.GetComponentInParent<Platform>();

            _body.velocity = Vector3.zero;

            //get current platform id
            var currentPlatform = _levelCreateManager.GetPlatformById(platform.PlatformId);

            //get next platform to jump on
            var nextPlatform = _levelCreateManager.GetPlatformById(platform.PlatformId - 1);

            //compute direction along XY surface
            if (nextPlatform == null) return;
            if (currentPlatform == null) return;

            var jumpDirection = (nextPlatform.transform.position - currentPlatform.transform.position).normalized;

            if(Random.Range(0,1.0f) > _jumpUpProbability)
            {
                _body.AddForce(Quaternion.Euler(0, Random.Range(-_jumpAngleMax, +_jumpAngleMax), 0) * jumpDirection * Random.Range(_jumpPowerRangeZ.x, _jumpPowerRangeZ.y)
                    + Vector3.up * Random.Range(_jumpPowerRangeY.x, _jumpPowerRangeY.y), ForceMode.Impulse);
            }
            else
            {
                _body.AddForce(Vector3.up * Random.Range(_jumpPowerRangeY.x, _jumpPowerRangeY.y), ForceMode.Impulse);
            }

            transform.forward = jumpDirection;

            /*
            if (_isFastJump)
            {
                _isFastJump = false;

                _currentSlowJumpNumber = -1;

                //get current platform id
                var currentPlatform = _levelCreateManager.GetPlatformById(platform.PlatformId);

                //get next platform to jump on
                var nextPlatform = _levelCreateManager.GetPlatformById(platform.PlatformId - 1);

                //compute direction along XY surface
                var jumpDirection = (nextPlatform.transform.position - currentPlatform.transform.position).normalized;

                _body.AddForce(Quaternion.Euler(0, Random.Range(-_jumpAngleMax, +_jumpAngleMax), 0) * jumpDirection * Random.Range(_jumpPowerRangeZ.x, _jumpPowerRangeZ.y)
                        + Vector3.up * Random.Range(_jumpPowerRangeY.x, _jumpPowerRangeY.y), ForceMode.Impulse);

                transform.forward = jumpDirection;
            }
            else
            {
                if (_currentSlowJumpNumber == -1)
                {
                    _isFastJump = Random.Range(0.0f, 1.0f) < _fastJumpProbability ? true : false;

                    if (!_isFastJump)
                    {
                        _currentSlowJumpNumber = (int)Random.Range(_slowJumpsNumber.x, _slowJumpsNumber.y);
                    }
                    else
                    {

                    }
                }
                else
                {
                    //perffom jump on the same platform
                    _body.AddForce(Vector3.up * Random.Range(_jumpPowerRangeY.x, _jumpPowerRangeY.y), ForceMode.Impulse);

                    _currentSlowJumpNumber--;

                    if (_currentSlowJumpNumber == -1)
                    {
                        _isFastJump = true;
                    }
                }
            }*/

            //platform.CompletePlatform();

            _animator.SetTrigger("Jump" + Random.Range(0, _jumpAnimationNumber));
        }
    }

    public void Death()
    {
        _body.velocity = Vector3.zero;
        //_body.isKinematic = true;
        _body.useGravity = false;
    }
}
