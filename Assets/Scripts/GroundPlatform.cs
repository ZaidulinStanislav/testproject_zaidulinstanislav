﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundPlatform : Platform
{
    [SerializeField] private float _sinkOffset;
    [SerializeField] private float _sinkTime;

    void Start()
    {
        PlatformType = PlatformType.Ground;
    }

    public override void CompletePlatform(Vector3 position, out bool isPerfect)
    {
        isPerfect = false;

        _completeParticles.Play();

        StartCoroutine(Sink(transform.position - Vector3.up * _sinkOffset));
    }

    private IEnumerator Sink(Vector3 targetMovePosition)
    {
        var startMovingTime = Time.time;
        var startMovingPosition = transform.position;

        var coef = 0.0f;
        while (coef < 1.0f)
        {
            coef = (Time.time - startMovingTime) / _sinkTime;

            transform.position = Vector3.Lerp(startMovingPosition, targetMovePosition, coef);

            yield return new WaitForFixedUpdate();
        }

        transform.position = targetMovePosition;
    }
}
