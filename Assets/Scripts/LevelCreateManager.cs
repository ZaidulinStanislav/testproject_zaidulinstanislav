﻿using FluffyUnderware.DevTools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FluffyUnderware.Curvy.Generator;
using FluffyUnderware.Curvy;
using FluffyUnderware.DevTools;
using FluffyUnderware.Curvy.Generator.Modules;

public class LevelCreateManager : MonoBehaviour
{
    [Header("General")]
    [SerializeField] private GroundPlatformCreateManager _groundPlatformCreateManager;
    [SerializeField] private Transform _waterObject;
    [SerializeField] private Transform _cloudsObject;
    [SerializeField] private LevelManager _levelManager;
    [SerializeField] private CurvyGenerator _splineLineGenerator;

    [Header("Level progress parameters")]
    [SerializeField] private Vector2 _levelPlatformNumberRange;
    [SerializeField] private float _levelPlatformNumberIncreaseValue;

    [Header("Level create parameters")]
    //[SerializeField] private Generator _plaformOffset;
    [SerializeField] private float _plaformOffset;
    [SerializeField] private float _plaformCurvedCreateMaxAngle;
    [SerializeField] private float _plaformLinearCreateMaxAngle;
    [SerializeField] private float _platformFlatHeightMax;
    [SerializeField] private float _platformFallHeightMax;
    [SerializeField] private Vector2 _curvedOneSidePlatformNumber;
    [SerializeField] private Vector2 _linearPlatformNumber;
    [SerializeField] private Vector2 _fallPlatformNumber;
    [SerializeField] private Vector2 _flatPlatformNumber;
    [SerializeField] private CurvySpline _spline;

    [SerializeField] private float[] _platformCreateProbabilities;
    [SerializeField] private int _platformTypeNumber;
    [SerializeField] private FragilePlatform _fragilePlatformPrefab;
    [SerializeField] private MovingPlatform _movingPlatformPrefab;
    [SerializeField] private TrampolinePlatform _trampolinePlatformPrefab;
    [SerializeField] private EndPlatform _endPlatformPrefab;
    [SerializeField] private Vector3 _endPlatformPosition;

    public int LevelPlatformNumber { get; private set; }
    public Platform GetPlatformById(int id) { return id > -1 && id < LevelPlatformNumber ? _platforms[id] : _platforms[0]; } 
    public Platform GetFirstPlatform() { return _platforms[LevelPlatformNumber-1]; } 
    private List<Platform> _platforms;
    private int _levelNumber;

    private bool _isLinearCreation = true;
    private bool _isFlatCreation = true;
    private float _curvedCreationSide = 1.0f;
    private int _creationNumberHorizontal;
    private int _creationNumberVertical;
    private Vector3 _levelCenterPosition;
    private float _platformCreateProbabilitiesSum;

    public void Initialize(int levelNumber, out Vector3 firstPlatformPosition, out Vector3 firstPlatformDierction)
    {
        ClearPlatforms();

        LevelPlatformNumber = (int) Mathf.Clamp((_levelPlatformNumberRange.x + _levelPlatformNumberIncreaseValue * levelNumber), 
            _levelPlatformNumberRange.x, _levelPlatformNumberRange.y);

        _levelNumber = levelNumber;

        foreach (var p in _platformCreateProbabilities)
        {
            _platformCreateProbabilitiesSum += p;
        }

        Vector2 levelMinMaxX;
        Vector2 levelMinMaxZ;

        CreateLevel(out firstPlatformPosition, out firstPlatformDierction, out levelMinMaxX, out levelMinMaxZ);

        var levelMaxRadius = Mathf.Max(Mathf.Abs(levelMinMaxX.x - _levelCenterPosition.x), Mathf.Abs(levelMinMaxX.y - _levelCenterPosition.x),
            Mathf.Abs(levelMinMaxZ.x - _levelCenterPosition.z), Mathf.Abs(levelMinMaxZ.y - _levelCenterPosition.z));

        _groundPlatformCreateManager.Initialize(_levelCenterPosition, levelMaxRadius);

        _waterObject.transform.position = new Vector3(_levelCenterPosition.x, _waterObject.transform.position.y, _levelCenterPosition.z);

        _cloudsObject.transform.position = _waterObject.transform.position + Vector3.up * 5.0f;

        _waterObject.transform.localScale = new Vector3(levelMaxRadius, 1, levelMaxRadius);
    }

    private void CreateLevel(out Vector3 firstPlatformPosition,out Vector3 firstPlatformDirection, out Vector2 minMaxX, out Vector2 minMaxZ)
    {
        _platforms = new List<Platform>();

        //Level creates from the end to the top in order to get right final height of the entire level
        //End platform position = 0 0 0

        //Create end platform
        var endPlatform = Instantiate(_endPlatformPrefab, _spline.transform);
        endPlatform.transform.position = _endPlatformPosition;
        endPlatform.gameObject.AddComponent<CurvySplineSegment>();
        endPlatform.Initialize(_levelManager);

        //Set initial parameters for the further creation process
        _isLinearCreation = Random.Range(0, 2) == 0 ? true : false;
        _isFlatCreation = Random.Range(0, 2) == 0 ? true : false;
        _curvedCreationSide = Random.Range(0, 2) == 0 ? +1 : -1;
        minMaxX = Vector2.zero;
        minMaxZ = Vector2.zero;

        _creationNumberHorizontal = (int)(_isLinearCreation ? Random.Range(_linearPlatformNumber.x, _linearPlatformNumber.y)
                                              : Random.Range(_curvedOneSidePlatformNumber.x, _curvedOneSidePlatformNumber.y));

        _creationNumberVertical = (int)(_isFlatCreation ? Random.Range(_flatPlatformNumber.x, _flatPlatformNumber.y)
                                              : Random.Range(_fallPlatformNumber.x, _fallPlatformNumber.y));

        var creationDirection = -Vector3.forward; // Direction from previous platform to current platform

        var prevPlatformPosition = endPlatform.transform.position;

        for (int i = 0; i < LevelPlatformNumber; i++)
        {
            //Create current platform
            Platform platform = null;
            PlatformType platformType = PlatformType.Trampoline;

            if (i != LevelPlatformNumber-1)
            {
                platformType = GetCreatedPlatformType();
                switch (platformType)
                {
                    case PlatformType.Fragile:
                        platform = Instantiate(_fragilePlatformPrefab, _spline.transform);
                        break;
                    case PlatformType.Moving:
                        var norm = Quaternion.Euler(0, 90, 0) * creationDirection;
                        platform = Instantiate(_movingPlatformPrefab, _spline.transform);
                        var movingPlatform = platform as MovingPlatform;
                        break;
                    case PlatformType.Trampoline:
                        platform = Instantiate(_trampolinePlatformPrefab, _spline.transform);
                        break;
                    default:
                        break;
                }
            }
            else
            {
                platform = Instantiate(_trampolinePlatformPrefab, _spline.transform);
            }

            platform.Initialize(i);

            platform.gameObject.AddComponent<CurvySplineSegment>();
            _platforms.Add(platform);

            Vector3 offset =
                Vector3.up * (_isFlatCreation                                                   //Offset along Y axis
                    ? Random.Range(_platformFlatHeightMax / 3.0f, _platformFlatHeightMax)
                    : Random.Range(_platformFallHeightMax / 3.0f, _platformFallHeightMax))
                + Quaternion.Euler(0, (_isLinearCreation                                        //Offset along XZ surface
                    ? Random.Range(-_plaformLinearCreateMaxAngle, _plaformLinearCreateMaxAngle)
                    : _curvedCreationSide * Random.Range(_plaformCurvedCreateMaxAngle / 3.0f, _plaformCurvedCreateMaxAngle)), 0)
                        * creationDirection * _plaformOffset;


            platform.transform.position = prevPlatformPosition + offset; // change position of current platform
            
            creationDirection = new Vector3(offset.x, 0, offset.z).normalized; //Update creation direction vector

            platform.transform.forward = -creationDirection; //change rotation of current platform

            prevPlatformPosition = platform.transform.position; //Update previous platform position

            //Update creation states
            _creationNumberHorizontal--;
            _creationNumberVertical--;

            if(_creationNumberHorizontal < 0)
            {
                _isLinearCreation = !_isLinearCreation;

                _creationNumberHorizontal = (int)(_isLinearCreation ? Random.Range(_linearPlatformNumber.x, _linearPlatformNumber.y)
                                              : Random.Range(_curvedOneSidePlatformNumber.x, _curvedOneSidePlatformNumber.y));
            }

            if (_creationNumberVertical < 0)
            {
                _isFlatCreation = !_isFlatCreation;

                _creationNumberVertical = (int)(_isFlatCreation ? Random.Range(_flatPlatformNumber.x, _flatPlatformNumber.y)
                                              : Random.Range(_fallPlatformNumber.x, _fallPlatformNumber.y));
            }

            //Expand level's bounds if it's need

            if (prevPlatformPosition.x > minMaxX.y) minMaxX.y = prevPlatformPosition.x;
            if (prevPlatformPosition.x < minMaxX.x) minMaxX.x = prevPlatformPosition.x;
            if (prevPlatformPosition.z > minMaxZ.y) minMaxZ.y = prevPlatformPosition.z;
            if (prevPlatformPosition.z < minMaxZ.y) minMaxZ.x = prevPlatformPosition.z;

            //Sum platform positions to fint level center
            _levelCenterPosition += prevPlatformPosition;

            //Make actions after platform is placed
            switch (platformType)
            {
                case PlatformType.Fragile:
                    
                    break;
                case PlatformType.Moving:
                    var norm = Quaternion.Euler(0, 90, 0) * creationDirection;
                    var movingPlatform = platform as MovingPlatform;
                    movingPlatform.StartMoving(norm);
                    break;
                case PlatformType.Trampoline:
                    
                    break;
                default:
                    break;
            }
        }

        //All platforms have been created
        //The first platform equals to the last created platform

        var firstPlatform = _platforms[_platforms.Count - 1];
        firstPlatformPosition = firstPlatform.transform.position;

        firstPlatformDirection = firstPlatform.transform.forward;

        _levelCenterPosition = _levelCenterPosition / LevelPlatformNumber;
    }

    private PlatformType GetCreatedPlatformType()
    {
        var rnd = Random.Range(0, _platformCreateProbabilitiesSum);

        for (int i = 0; i < _platformTypeNumber; i++)
        {
            if(rnd < _platformCreateProbabilities[i])
            {
                return (PlatformType)i;
            }
            else
            {
                rnd -= _platformCreateProbabilities[i];
            }
        }

        return PlatformType.Fragile;
    }

    private void ClearPlatforms()
    {
        if (_platforms != null)
        {
            foreach (var p in _platforms)
            {
                if (p != null && p.gameObject != null) Destroy(p.gameObject);
            }
        }
    }
}
