﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShadow : MonoBehaviour
{
    [SerializeField] private Transform _playerShadow;
    [SerializeField] private Transform _playerLandingPoint;
    [SerializeField] private float _playerShadowMaxSizeY;
    [SerializeField] private CapsuleCollider _collider;

    [SerializeField] private float _landingPointOffsetY = 0.3f;

    [SerializeField] private MeshRenderer _playerShadowMeshRenderer;
    [SerializeField] private Material _positivePlayerShadowMaterial;
    [SerializeField] private Material _negativePlayerShadowMaterial;


    private void Awake()
    {
        _collider.height = _playerShadowMaxSizeY;
        _collider.center = new Vector3(0, -_playerShadowMaxSizeY / 2.0f, 0);
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.CompareTag("Platform"))
        {
            var landingPoint = new Vector3(transform.position.x,
                other.transform.TransformPoint(Vector3.up / 2.0f).y,
                transform.position.z);

            _playerLandingPoint.gameObject.SetActive(true);
            _playerLandingPoint.transform.position = landingPoint;

            var disatanceY = (transform.position - landingPoint).y;

            _playerShadow.transform.localScale = new Vector3(_playerShadow.transform.localScale.x, disatanceY/2.0f, _playerShadow.transform.localScale.z);
            _playerShadow.transform.localPosition =  -Vector3.up * (disatanceY / 2.0f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Platform"))
        {
            _playerShadowMeshRenderer.material = _positivePlayerShadowMaterial;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Platform"))
        {
            _playerLandingPoint.gameObject.SetActive(false);

            _playerShadowMeshRenderer.material = _negativePlayerShadowMaterial;

            _playerShadow.transform.localScale = new Vector3(_playerShadow.transform.localScale.x, _playerShadowMaxSizeY/2.0f, _playerShadow.transform.localScale.z);
            _playerShadow.transform.localPosition = -Vector3.up * (_playerShadowMaxSizeY / 2.0f);
        }
    }

    public void ForceDeactivate()
    {
        _playerLandingPoint.gameObject.SetActive(false);

        _playerShadowMeshRenderer.material = _negativePlayerShadowMaterial;

        _playerShadow.transform.localScale = new Vector3(_playerShadow.transform.localScale.x, _playerShadowMaxSizeY / 2.0f, _playerShadow.transform.localScale.z);
        _playerShadow.transform.localPosition = -Vector3.up * (_playerShadowMaxSizeY / 2.0f);
    }
}
