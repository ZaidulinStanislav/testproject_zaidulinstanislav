﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : Platform
{
    [SerializeField] private float _movingTime;
    [SerializeField] private float _waitTime;
    [SerializeField] private float _movingOffset;
    [SerializeField] private Transform _movingObject;

    private bool _isMovingRight;
    private Vector3 _normDirection;
    //private Vector3 _targetMovePosition;

    void Start()
    {
        PlatformType = PlatformType.Moving;
    }

    public void StartMoving(Vector3 normDirection)
    {
        _normDirection = normDirection;

        _isMovingRight = Random.Range(0, 2) == 0 ? true : false;

        StartCoroutine(MoveCoroutine(_movingObject.position + (_isMovingRight ? +1 : -1) * normDirection * _movingOffset / 2.0f));
    }

    private IEnumerator MoveCoroutine(Vector3 targetMovePosition)
    {
        var startMovingTime = Time.time;
        var startMovingPosition = _movingObject.position;

        var coef = 0.0f;
        while(coef < 1.0f)
        {
            coef = (Time.time - startMovingTime) / _movingTime;

            _movingObject.position = Vector3.Lerp(startMovingPosition, targetMovePosition, coef);

            yield return new WaitForFixedUpdate();
        }

        _movingObject.position = targetMovePosition;

        yield return new WaitForSeconds(_waitTime);

        _isMovingRight = !_isMovingRight;
        //Debug.Log(_isMovingRight);
        //Debug.DrawLine(transform.position, transform.position + ((_isMovingRight ? +1 : -1) * _normDirection * _movingOffset)*100);

        StartCoroutine(MoveCoroutine(targetMovePosition + (_isMovingRight ? +1 : -1) * _normDirection * _movingOffset));
    }
}
