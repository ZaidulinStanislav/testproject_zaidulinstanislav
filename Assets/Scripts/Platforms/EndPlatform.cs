﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndPlatform : MonoBehaviour
{
    [SerializeField] private ParticleSystem[] _completeParticles;

    private LevelManager _levelManager;

    private bool isCompleted;

    public void Initialize(LevelManager levelManager)
    {
        _levelManager = levelManager;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (isCompleted) return;

        if (other.CompareTag("Player"))
        {
            isCompleted = true;

            _levelManager.CompleteLevel();

            foreach (var p in _completeParticles)
            {
                p.Play();
            }
        }
    }
}
