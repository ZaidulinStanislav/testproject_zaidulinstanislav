﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public enum PlatformType
{
    Fragile,
    Moving,
    Trampoline,
    Ground
}

public class Platform : MonoBehaviour
{
    public PlatformType PlatformType { get; protected set; }
    public int PlatformId { protected set; get; }

    [SerializeField] private TextMeshProUGUI _platformIdText;
    [SerializeField] protected ParticleSystem _completeParticles;
    [SerializeField] private Animator _platformAnimator;
    [SerializeField] private float _perfectRadius = 1.0f;
    [SerializeField] private Transform PlatformTransform;

    protected bool _isPerfectLanding;
    protected bool _isCompleted;

    public virtual void Initialize(int platformId)
    {
        PlatformId = platformId;

        _platformIdText.text = PlatformId.ToString();
    }

    public virtual void CompletePlatform(Vector3 playerPosition, out bool isPerfect)
    {
        var vec = (playerPosition - PlatformTransform.position);
        vec.y = 0;

        _isPerfectLanding = vec.magnitude < _perfectRadius;

        if (!_isCompleted) isPerfect = _isPerfectLanding; else isPerfect = false;

        _completeParticles.Play();

        _platformAnimator.SetTrigger("Complete");
    }
}
