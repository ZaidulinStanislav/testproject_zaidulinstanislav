﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragilePlatform : Platform
{
    //[Header("General")]

    [Header("Destroy parameters")]
    [SerializeField] private int _initialHealth;
    [SerializeField] private float _delayBeforeDestroyObject;
    [SerializeField] private Vector2 _destroySpreadPower;
    [SerializeField] private float _destroySpreadTorquePower;
    [SerializeField] private float[] _partsOffsetValuePerHealth;
    [SerializeField] private GameObject[] _platformUpParts;
    [SerializeField] private GameObject[] _platformDownParts;
    [SerializeField] private GameObject[] _wings;
    [SerializeField] private GameObject[] _columns;

    private int _health;
    private bool _isDestroyed;
    private BoxCollider _collider;

    private void Awake()
    {
        _collider = GetComponent<BoxCollider>();
    }

    public void Start()
    {
        _health = _initialHealth;
        _isDestroyed = false;

        PlatformType = PlatformType.Fragile;
    }

    public bool DecreaseHealth()
    {
        if (_isDestroyed) return true;

        _health--;
        if (_health > 0)
        {
            MovePlatformParts(_partsOffsetValuePerHealth[_initialHealth - _health]);
        }
        else
        {
            _isDestroyed = true;

            DestroyPlatform();
        }

        return _isDestroyed;
    }

    private void MovePlatformParts(float offsetValue)
    {
        var platformCenter = transform.position;
        var offsetYValue = offsetValue * 2.0f;
        var offsetRotationValue = offsetValue * 5.0f;

        foreach (var p in _platformUpParts)
        {
            var offsetVec = (p.transform.position - platformCenter).normalized * offsetValue;
            offsetVec.y = Random.Range(-offsetYValue, offsetYValue);

            p.transform.position += offsetVec;
            p.transform.localRotation *= Quaternion.Euler(Random.Range(-offsetRotationValue, offsetRotationValue),
                Random.Range(-offsetRotationValue, offsetRotationValue), Random.Range(-offsetRotationValue, offsetRotationValue));
        }

        foreach (var p in _platformDownParts)
        {
            var offsetVec = (p.transform.position - platformCenter).normalized * offsetValue;
            offsetVec.y = Random.Range(-offsetYValue, offsetYValue);

            p.transform.position += offsetVec;
            p.transform.localRotation *= Quaternion.Euler(Random.Range(-offsetRotationValue, offsetRotationValue),
                Random.Range(-offsetRotationValue, offsetRotationValue), Random.Range(-offsetRotationValue, offsetRotationValue));
        }
    }

    private void DestroyPlatform()
    {
        var platformCenter = transform.position;

        foreach (var p in _platformUpParts)
        {
            var body = p.AddComponent<Rigidbody>();
            //p.AddComponent<BoxCollider>();

            body.AddForce((body.transform.position - platformCenter).normalized * Random.Range(_destroySpreadPower.x, _destroySpreadPower.y),
                ForceMode.Impulse);

            body.AddTorque(new Vector3(Random.Range(-_destroySpreadTorquePower, _destroySpreadTorquePower),
                Random.Range(-_destroySpreadTorquePower, _destroySpreadTorquePower),
                Random.Range(-_destroySpreadTorquePower, _destroySpreadTorquePower)), ForceMode.Impulse);
        }

        foreach (var p in _platformDownParts)
        {
            var body = p.AddComponent<Rigidbody>();
            //p.AddComponent<BoxCollider>();

            body.AddForce((body.transform.position - platformCenter).normalized * Random.Range(_destroySpreadPower.x, _destroySpreadPower.y),
                ForceMode.Impulse);
        }

        foreach (var p in _wings)
        {
            var body = p.AddComponent<Rigidbody>();
            //p.AddComponent<BoxCollider>();

            body.AddForce((body.transform.position - platformCenter).normalized * Random.Range(_destroySpreadPower.x, _destroySpreadPower.y),
                ForceMode.Impulse);
        }

        foreach (var p in _columns)
        {
            var body = p.AddComponent<Rigidbody>();
            //p.AddComponent<BoxCollider>();

            body.AddForce((body.transform.position - platformCenter).normalized * Random.Range(_destroySpreadPower.x, _destroySpreadPower.y),
                ForceMode.Impulse);
        }


        _collider.enabled = false;

        Invoke("DestroyObject", _delayBeforeDestroyObject);
    }

    private void DestroyObject()
    {
        //Destroy(gameobject);
        Destroy(this);
    }
}
